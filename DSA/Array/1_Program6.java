//Given an array of size N
//Reverse the array without using extra space
//Space complexity: O(1)
//Arr: [8,4,1,3,9,2,6,7]
//Output: 
//Arr: [7,6,2,9,3,1,4,8]

class ReverseArray_6{

	public static void main(String[] args){

		int arr[] = new int[]{5,1,4,8,0,1,3,8};
		int N = arr.length;

		for(int i = 0; i<N; i++){

			System.out.print(arr[i]+", ");
		}
		System.out.println();

		int temp = 0;
		for(int i = 0; i<N/2; i++){

			temp = arr[i];
			arr[i] = arr[N-1-i];
			arr[N-1-i] = temp;
		}
		
		for(int i = 0; i<N; i++){

			System.out.print(arr[i]+", ");
		}
		System.out.println();
	}
}

//Time Complexity: O(n/2) ==> O(n)
//Space Complexity: O(1)

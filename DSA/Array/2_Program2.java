
//Given an array of size N and Q number of queries
//Query contains 2 parameters (s,e) s => start index and e => end index
//For all queries print the sum of all elements from index s to index e
//Arr: [-3,6,2,4,5,2,8,-9,3,1]
//N: 10
//Q: 3
//Queries	s	e	sum
//query1:	1	3	12
//query2:	2	7	12
//query3:	1	1	6

import java.util.Scanner;

class QuerySumPrefixSum_9{

	public static void main(String[] args){

		int arr[] = new int[]{-3,6,2,4,5,2,8,-9,3,1};
		int N = arr.length;
		int pArr[] = new int[N];
		pArr[0] = arr[0];
		for(int i = 1; i<N; i++){

			pArr[i] = pArr[i-1] + arr[i];
		}
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number of queries: ");
		int Q = sc.nextInt();
		for(int i = 0; i<Q; i++){

			System.out.println("Enter starting index:");
			int start = sc.nextInt();
			if(start<0){

				System.out.println("Start index cannot be negative!");
				break;
			}
			System.out.println("Enter ending index:");
			int end = sc.nextInt();
			if(end>=N){

				System.out.print("End index is out of bounds!");
				break;
			}
			if(start == 0){
				System.out.println("Query"+(i+1)+": "+pArr[end]);
			}else{
				System.out.println("Query"+(i+1)+": "+(pArr[end]-pArr[start-1]));
			}
		}
	}
}

//Given an array of size N
//Return the count of pairs (i,j) with Arr[i] + Arr[j] = k
//Arr: [3,5,2,1,-3,7,8,15,6,13]
//N: 10
//K: 10
//Output: 6

class CountPairsEqualToSum_4{

	public static void main(String[] args){

		int arr[] = new int[]{3,5,2,1,-3,7,8,15,6,13};
		int N = arr.length;
		int K = 10;
		int count = 0;
		for(int i = 0; i<N; i++){

			for(int j = 0; j<N; j++){	//Can use for(int j = i+1; j<N; j++) to reduce iterations

				if(i == j)
					continue;
				if((arr[i]+arr[j]) == K)
					count++;
			}
		}
		System.out.println("Count of pairs: "+count);
	}
}

//Time Complexity: O(n^2)
//Space Complexity: O(1)

//Given an integer array of size N
//Count number of elements having atleast 1 element greater than itself
//Arr: [2,5,1,4,8,0,8,1,3,8]
//N: 10
//Output: 7

class CountAtleast1Greater_2{

	public static void main(String[] args){

		int arr[] = new int[]{2,5,1,4,8,0,1,3,8};
		int N = arr.length;
		int count = 0;
		for(int i = 0; i<N; i++){

			for(int j = 0; j<N; j++){

				if(arr[i]<arr[j]){

					count++;
					break;
				}
			}
		}
		System.out.println("Count: "+count);
	}
}

//Time Complexity: O(n^2)
//Space Complexity: O(1)

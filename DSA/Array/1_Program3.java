//Given an integer array of size N
//Count number of elements having atleast 1 element greater than itself
//Arr: [2,5,1,4,8,0,8,1,3,8]
//N: 10
//Output: 7

class CountAtleast1Greater_3{

	public static void main(String[] args){

		int arr[] = new int[]{2,5,1,4,8,0,1,3,8};
		int N = arr.length;
		int largestElement = Integer.MIN_VALUE;	//Do not write arr[0] here as it is a test case in platforms like LeetCode
							//MIN_VALUE is a company standard
		for(int i = 0; i<N; i++){

			if(arr[i]>largestElement)
				largestElement = arr[i];
		}
		int count = 0;
		for(int i = 0; i<N; i++){

			if(arr[i] == largestElement){

				count++;
			}
		}
		System.out.println("Count: "+(N-count));
	}
}

//Time Complexity: O(n)
//Space Complexity: O(1)

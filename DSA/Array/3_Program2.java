//Given an array of size N
//Build an array leftmax of size N
//Leftmax of i contains the maximum for the index 0 to the index i
//Arr: [-3,6,2,4,5,2,8,-9,3,1]
//N: 10
//LeftMax: [-3,6,6,6,6,6,8,8,8,8]

//Optimised Approach
class ArrayLeftMaxOptimized{

	public static void main(String[] args){

		int arr[] = new int[]{-3,6,2,4,5,2,8,-9,3,1};
		int N = 10;
		int leftMax[] = new int[N];
		int max = Integer.MIN_VALUE;
		for(int i = 0; i<N; i++){

			if(arr[i]>max)
				max = arr[i];
			leftMax[i] = max;
		}
		System.out.println("Leftmax array:");
		for(int i = 0; i<N; i++){

			System.out.print(leftMax[i]+" ");
		}
		System.out.println();
	}
}
//Time Complexity: O(n)
//Space Complexity: O(1)

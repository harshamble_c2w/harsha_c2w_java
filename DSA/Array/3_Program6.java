//Given a character array (lowercase)
//return count of pair (i,j) such that
//a) arr[i] = 'a'
//   arr[j] = 'g'
//Arr: [a,b,e,g,a,g]
//output: 3

//More Optimized
class PairsAGMoreOptimized{

	public static void main(String[] args){

		char arr[] = new char[]{'a','b','e','g','a','g'};
		int count = 0;
		int temp = 0;
		int itr = 0;
		for(int i = 0; i<arr.length; i++){

			if(arr[i]=='a')
				temp++;
			else if(arr[i] == 'g')
				count+=temp;
			itr++;
		}
		System.out.println("Count of pairs: "+count);
		System.out.println("Iterations: "+itr);
	}
}
//Time complexity: O(n)
//Space complexity: O(1)

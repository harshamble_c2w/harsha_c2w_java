//Given an array of size N
//Reverse the array such that T.C = O(n) and S.C = O(n)

class ReverseArray_5{

	public static void main(String[] args){

		int arr[] = new int[]{2,5,1,4,8,0,1,3,8};
		int N = arr.length;
		int arrRev[] = new int[N];
		for(int i = 0; i<N; i++){

			arrRev[i] = arr[N-i-1];
		}
		for(int i = 0; i<N; i++){

			System.out.print(arr[i]+", ");
		}
		System.out.println();
		for(int i = 0; i<N; i++){

			System.out.print(arrRev[i]+", ");
		}
		System.out.println();
	}
}

//Time Complexity: O(n)
//Space Complexity: O(n)

//Given an array of size N and Q number of queries
//Query contains 2 parameters (s,e) s => start index and e => end index
//For all queries print the sum of all elements from index s to index e
//Arr: [-3,6,2,4,5,2,8,-9,3,1]
//N: 10
//Q: 3
//Queries	s	e	sum
//query1:	1	3	12
//query2:	2	7	12
//query3:	1	1	6

import java.util.Scanner;
class QuerySum_8{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		int arr[] = new int[]{-3,6,2,4,5,2,8,-9,3,1};
		int N = arr.length;
		System.out.println("Enter the number of queries: ");
		int Q = sc.nextInt();
		//System.out.println("Queries  \ts\te\tsum");
		for(int i = 0; i<Q; i++){

			System.out.println("Enter starting index: ");
			int start = sc.nextInt();
			if(start<0){
				System.out.println("Start index cannot be negative!");
				break;
			}
			System.out.println("Enter ending index: ");
			int end = sc.nextInt();
			if(end>=N){
				System.out.println("Ending index out of bounds!");
				break;
			}
			int sum = 0;
			for(int j = start; j<=end; j++){
				sum+=arr[j];
			}
			System.out.println("Query"+(i+1)+"\t"+start+"\t"+end+"\t"+sum);
		}
	}
}
//Time complexity: O(Q*N)
//Space complexity: O(1)

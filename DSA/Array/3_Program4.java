//Given a character array (lowercase)
//return count of pair (i,j) such that
//a) arr[i] = 'a'
//   arr[j] = 'g'
//Arr: [a,b,e,g,a,g]
//output: 3

//Brute Force approach
class PairsAG{

	public static void main(String[] args){

		char arr[] = new char[]{'a','a','e','g','a','g'};
		int count = 0;
		int itr = 0;
		for(int i = 0; i<arr.length; i++){

			if(arr[i]=='a'){

				for(int j = i+1; j<arr.length; j++){

					if(arr[j] == 'g')
						count++;
					itr++;
				}
			}
		}
		System.out.println("Count of pairs: "+count);
		System.out.println("Iterations: "+itr);
	}
}
//Time complexity: O(n^2)
//Space complexity: O(1)

//Given an array of size N. Print all elements
//Arr: [5,6,2,3,1,9]
//N: 6

class PrintAllElements_1{

	public static void main(String[] args){

		int arr[] = new int[]{5,6,2,3,1,9};
		int N = arr.length;
		for(int i = 0; i<N; i++){

			System.out.println(arr[i]);
		}
	}
}

//Time Complexity: O(n)	Linear
//Space Complexity: O(1)  Constant

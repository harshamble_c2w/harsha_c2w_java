
//Given an array of N integers
//Find summation of pairs 1st, last 2nd, 2nd last ...

class SumOfPairs_7{
	
	public static void main(String[] args){

		int arr[] = new int[]{1,2,3,4,5,6,7,8};
		int N = arr.length;
		int sum = 0;
		for(int i = 0; i<N/2; i++){

			sum = arr[i] + arr[N-i-1];
			System.out.println("Sum of elements at indexes "+i+", "+(N-i-1)+" : "+sum);
		}
		if(N%2 == 1){

			System.out.println("Element at index "+(N/2)+" : "+arr[N/2]);
		}
	}
}
//Time complexity: O(n)
//Space complexity: O(1)

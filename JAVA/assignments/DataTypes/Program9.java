//Write a program to print the temperature of the Air Conditioner in degrees and also print the standard room temperature using appropriate data types
//temp of AC: byte
//temp of room: float


class Demo{
	public static void main(String[] args){
		byte temp_AC = 22;
		float temp_room = 18.4f;
		System.out.println("Temperature of AC: "+temp_AC);
		System.out.println("Temperature of room: "+temp_room);
	}
}

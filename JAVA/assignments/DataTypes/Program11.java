//Write a program to print the value of gravity and print the letter used to represent the acceleration due to gravity
//value: float
//letter: char


class Demo{
	public static void main(String[] args){
		float value = 9.8f;
		char gravitational_acc = 'g';
		System.out.println(gravitational_acc+"= "+value);
	}
}

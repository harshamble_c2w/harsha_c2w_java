//Write a program to print the current and voltage
//current: float
//voltage: float

class Demo{
	public static void main(String[] args){
		float current = 3.1f;
		float voltage = 220f;
		System.out.println("Current: "+current);
		System.out.println("Voltage: "+voltage);
	}
}

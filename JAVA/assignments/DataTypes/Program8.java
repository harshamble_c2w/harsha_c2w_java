//Write a program to print the percentage and grade of a student
//percentage: float
//grade: char


class Demo{
	public static void main(String[] args){
		float percentage = 91.17f;
		char grade = 'O';
		System.out.println("Percentage: "+percentage);
		System.out.println("Grade: "+grade);
	}
}

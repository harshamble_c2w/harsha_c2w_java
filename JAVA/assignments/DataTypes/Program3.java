//A scientist is working on his research and wants to find an area using pi. (Write a code to print the value of pi angle of triangle). Scientists are also eager to print the highest degree of angle of circle (360 degree).
//pi: float


class Demo{
	public static void main(String[] args){
		float pi = 3.14f;
		System.out.println("Value of pi: "+pi);
	}
}

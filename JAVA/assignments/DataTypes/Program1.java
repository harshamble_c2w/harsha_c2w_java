//A student is in class 11th and there are 12 divisions of the class, which data type is feasible to print the class and division?
//class: byte
//division: char


class Demo{
	public static void main(String[] args){
		byte classs = 11;
		char division1 = 'A';
		char division2 = 'B';
		char division3 = 'C';
		char division4 = 'D';
		char division5 = 'E';
		char division6 = 'F';
		char division7 = 'G';
		char division8 = 'H';
		char division9 = 'I';
		char division10 = 'J';
		char division11 = 'K';
		char division12 = 'L';
		System.out.println("Class: "+classs);
		System.out.println("Division: "+division1);
	}
}

//Write a program to print the box office collection and imdb rating of a movie
//box office collection: long
//imdb rating: float


class Demo{
	public static void main(String[] args){
		long box_office_collection = 9000000000l;
		float imdb_rating = 6.5f;
		System.out.println("Box office collection: "+box_office_collection);
		System.out.println("Imdb rating: "+imdb_rating);
	}
}

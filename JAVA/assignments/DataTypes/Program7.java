//A person is trying to save the world data he wants to stroe the world's population and also wants to store India's population. (Print the world's and India's population using appropriate data types)
//India's population: int
//world's population: long


class Demo{
	public static void main(String[] args){
		int populationIndia = 1428627663;
		long populationWorld = 8045311447l;
		System.out.println("India's population: "+populationIndia);
		System.out.println("World's population: "+populationWorld);
	}
}

//A person is storing date, month and year in variables. (Write a code to print the date, month, year and also print tthe total seconds in a day, month and year)
//date: byte, seconds in day: int
//month: byte, seconds in month: int
//year: byte, seconds in year: int


class Demo{
	public static void main(String[] args){
		byte date = 28;
		byte month = 1;
		short year = 2024;
		int seconds_day = 86400;
		int seconds_month = 2592000;
		int seconds_year = 946080000;
		System.out.println("Day: "+date);
		System.out.println("Month: "+month);
		System.out.println("Year: "+year);
		System.out.println("Seconds in a day: "+seconds_day);
		System.out.println("Seconds in a month: "+seconds_month);
		System.out.println("Seconds in a year: "+seconds_year);
	}
}

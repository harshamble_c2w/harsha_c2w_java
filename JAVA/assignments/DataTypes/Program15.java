//Write a code for a real time example in which we can use 5 different data types


class Demo{
	public static void main(String[] args){
		float marks = 90.5f;
		char grade = 'O';
		byte credits = 30;
		boolean passed = true;
		int rollnumber = 305010;
		System.out.println("Roll number: "+rollnumber);
		System.out.println("Marks: "+marks);
		System.out.println("Grade: "+grade);
		System.out.println("Credits gained: "+credits);
		System.out.println("Passed? "+passed);
	}
}

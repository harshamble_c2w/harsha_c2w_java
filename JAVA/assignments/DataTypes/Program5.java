//A person is calculating the count of a button on a keyboard. (Write a code to print the count of button and also wants to print the cost of his keyboard)
//count of button: byte
//price of keyboard: int


class Demo{
	public static void main(String[] args){
		byte button_count = 101;
		int keyboard_price = 799;
		System.out.println("Button count: "+button_count);
		System.out.println("Price of keyboard: "+keyboard_price);
	}
}

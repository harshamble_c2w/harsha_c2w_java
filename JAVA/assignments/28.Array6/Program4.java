import java.util.Scanner;
class CommonElements {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the size of arrays: ");
        int size = sc.nextInt();
        int arr1[] = new int[size];
        System.out.println("Start entering elements of array1:");
        for(int i = 0; i<arr1.length; i++){
            System.out.println("Enter element "+(i+1)+": ");
            arr1[i] = sc.nextInt();
        }

        int arr2[] = new int[size];
        System.out.println("\nStart entering elements of array2:");
        for(int i = 0; i<size; i++){
            System.out.println("Enter element "+(i+1)+": ");
            arr2[i] = sc.nextInt();
        }

        System.out.print("\nCommon elements are: ");
        for(int i = 0; i<size; i++){
            for(int j = 0; j<size; j++){
                if(arr1[i] == arr2[j]){
                    System.out.print(arr1[i]+", ");
                }
            }
        }
        System.out.println();
    }
}
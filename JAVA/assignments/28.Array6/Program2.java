import java.util.Scanner;
class SumAndCountOfPrime {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the size of array: ");
        int size = sc.nextInt();
        int arr[] = new int[size];
        
        for(int i = 0; i<arr.length; i++){
            System.out.println("Enter element "+(i+1)+": ");
            arr[i] = sc.nextInt();
        }
        
        int sumPrime = 0, countPrime = 0;

        for(int i = 0; i<arr.length; i++){
            if(isPrime(arr[i])){
                sumPrime+=arr[i];
                countPrime++;
            }
        }

        System.out.println("Sum of prime numbers in given array is: "+sumPrime+"\nCount of prime numbers in given array is: "+countPrime);
    }
    static boolean isPrime(int num){
        for(int i = 2; i*i<=num; i++){
            if(num%i == 0)
                return false;
        }
        return true;
    }
}

import java.util.Scanner;
class MergeTwoArrays {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the size of array1: ");
        int size1 = sc.nextInt();
        int arr1[] = new int[size1];
        
        for(int i = 0; i<arr1.length; i++){
            System.out.println("Enter element "+(i+1)+": ");
            arr1[i] = sc.nextInt();
        }

        System.out.println("Enter the size of array2: ");
        int size2 = sc.nextInt();
        int arr2[] = new int[size2];
        
        for(int i = 0; i<arr2.length; i++){
            System.out.println("Enter element "+(i+1)+": ");
            arr2[i] = sc.nextInt();
        }

        int mergedArray[] = new int[size1+size2];
        for(int i = 0; i<mergedArray.length; i++){
            if(i<size1){
                mergedArray[i] = arr1[i];
            }else{
                mergedArray[i] = arr2[i-size1];
            }
        }

        System.out.print("| ");
        for(int i = 0; i<mergedArray.length; i++){
            System.out.print(mergedArray[i]+" | ");
        }
        System.out.println();
    }
}
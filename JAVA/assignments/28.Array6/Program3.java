import java.util.Scanner;
class CheckCountOfKey {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the size of array: ");
        int size = sc.nextInt();
        int arr[] = new int[size];
        
        for(int i = 0; i<arr.length; i++){
            System.out.println("Enter element "+(i+1)+": ");
            arr[i] = sc.nextInt();
        }

        int key;
        System.out.println("Enter the key: ");
        key = sc.nextInt();
        int count = 0;
        for(int i = 0; i<arr.length; i++){
            if(arr[i] == key)
                count++;
        }

        if(count > 2){
            for(int i = 0 ; i<arr.length; i++){
                if(arr[i] == key){
                    arr[i]=arr[i]*arr[i]*arr[i];
                }
            }
        }

        System.out.print("Final array:\n| ");
        for(int i = 0; i<arr.length; i++){
            System.out.print(arr[i]+" | ");
        }
        System.out.println();
    }
}
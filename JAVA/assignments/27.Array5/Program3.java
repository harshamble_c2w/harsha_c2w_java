//if array is palindrome or not

import java.util.Scanner;

class Palindrome{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size of array:");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Start entering the elements");
		for(int i = 0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}

		int rev[] = new int[size];
		for(int i = 0; i<arr.length; i++){

			rev[i] = arr[arr.length-i-1];
		}

		boolean palindrome = true;
		for(int i = 0; i<arr.length; i++){

			if(arr[i]!=rev[i]){

				palindrome = false;
				break;
			}
		}

		if(palindrome==true)
			System.out.println("The given array is a palindrome array");
		else
			System.out.println("The given array is not a palindrome array");
	}
}

//second minimum in array

import java.util.Scanner;

class SecondMinimum{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size of array:");
		int size = sc.nextInt(); 
		while(size<=0){
			
			System.out.println("Please enter size greater than 0");
			size = sc.nextInt();
		}
		int arr[] = new int[size];

		System.out.println("Start entering the elements");
		for(int i = 0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}

		int minElement = arr[0];
		//to find minimum element
		for(int i = 0; i<arr.length; i++){
	
			if(arr[i]<minElement)
				minElement = arr[i];
		}

		int minElement2 = 123456789;	//any large number
		for(int i = 0; i<arr.length; i++){

			if(arr[i]==minElement)
				continue;
			if(arr[i]<minElement2)
				minElement2 = arr[i];
		}
		if(minElement2!=123456789)
			System.out.println("The second minimum element in array is: "+minElement2);
		else
			System.out.println("There is no second minimum element in array");
	}
}

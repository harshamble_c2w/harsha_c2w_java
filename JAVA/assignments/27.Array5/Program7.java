//print composite numbers in array

import java.util.Scanner;

class Composite{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size of array:");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Start entering the elements");
		for(int i = 0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}

		System.out.print("Composite numbers in array are: ");
		for(int i = 0; i<arr.length; i++){

			int factors = 0;
			for(int j = 1; j<=arr[i]; j++){

				if(arr[i]%j==0)
					factors++;
			}
			if(factors==2)
				continue;
			System.out.print(arr[i]+", ");
		}
		System.out.println();
	}
}

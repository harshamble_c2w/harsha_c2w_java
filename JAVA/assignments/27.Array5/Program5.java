//count of digits in elements in array

import java.util.Scanner;

class NumberOfDigits{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size of array:");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Start entering the elements");
		for(int i = 0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}

		for(int i = 0; i<arr.length; i++){

			int temp = arr[i];
			int digits = 0;
			while(temp>0){

				digits++;
				temp/=10;
			}
			System.out.print(digits+", ");
		}
		System.out.println();
	}
}

//find out first suplicate element and return its index

import java.util.Scanner;

class FirstDuplicate{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size of array:");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Start entering the elements");
		for(int i = 0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}

		for(int i = 0; i<arr.length; i++){

			for(int j = i+1; j<arr.length; j++){

				//if(i==arr.length)
				//	break;
				//else{

					if(arr[i]==arr[j]){

						System.out.println("First duplicate element present at index: "+i);
						return;
					}
				//}
			}
		}

		System.out.println("There are no duplicate elements in the array");
	}
}

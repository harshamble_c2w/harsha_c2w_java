//check whether given array is in ascending order

import java.util.Scanner;

class AscendingOrder{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size of array:");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Start entering the elements");
		for(int i = 0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}

		for(int i = 1; i<arr.length; i++){
			if(arr[i]<arr[i-1]){
			
				System.out.println("The given array is not in ascending order");
				return;
			}
		}
		System.out.println("The given array is in ascending order");
	}
}

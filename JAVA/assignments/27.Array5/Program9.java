//take number as input from user
//store the digits in an array by increasing each digit by 1

import java.util.Scanner;

class StoreDigitIncrementBy1{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a number:");
		long num = sc.nextLong();

		int digits = 0;
		long temp = num;
		while(temp>0){

			digits++;
			temp/=10;
		}
		
		int arr[] = new int[digits];

		temp = num;
		long rev = 0;
		while(temp>0){

			long rem = temp%10;
			rev = rev*10 + rem;
			temp/=10;
		}

		for(int i = 0; i<arr.length; i++){

			int rem = (int)(rev%10);
			arr[i] = rem+1;
			rev/=10;
		}

		for(int i = 0; i<arr.length; i++){

			System.out.print(arr[i]+", ");
		}
		System.out.println();
	}
}

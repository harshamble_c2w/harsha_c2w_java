//first prime number in array

import java.util.Scanner;

class FirstPrime{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size of array:");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Start entering the elements");
		for(int i = 0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}

		for(int i = 0; i<arr.length; i++){

			int factors = 0;
			for(int j = 1; j<=arr[i]; j++){

				if(arr[i]%j==0)
					factors++;
			}
			if(factors==2){
			
				System.out.println("First prime number found at index: "+i);
				return;
			}
		}
		System.out.println("There are no prime numbers in given array");
	}
}

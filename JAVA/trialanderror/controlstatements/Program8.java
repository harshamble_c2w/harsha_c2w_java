class ForSwitchDemo{

	public static void main(String[] args){

		int i = 0;		//allowed: int, char, string
		for(;;i++){

			switch(i){

				case i:
					System.out.println("New");
				default:
					System.out.println();
			}

			if(i==10){
				break;
			}
		}
	}
}

//In JAVA, only declaration is not allowed
//any declared variable must be assigned a value before it is to be used

class DeclarationDemo {

	public static void main(String[] args){
		
		int x;
		int y = 10;
		//System.out.println(x);	//error: variable might not be initialized
		//x++;
		System.out.println(y);
		x = 20;
		System.out.println(x);
	}
}

class ScopeDemo{

	public static void main(String[] args){

		int i = 0;
		{
			
			int i = 10;	//error: variable 'i' is already defined in method main 
		}
	}
}

//for loop and while loop are very similar
//only the positioning of initiaization, condition, increment/decrement is different

/* 
  for(initialization; condition; increment/decrement){
      //body
  }
 
  initialization
  while(condition){
      //body
      increment/decrement
  }
 */

class ForDemo{

	public static void main(String[] args){

		int i = 1;
		for(;i<=5;++i){

			System.out.println("Hello: "+i);
			//++i;	//no error
		}
	}
}

//even if we declare a variable datatype to be 'short' or 'byte', JAVA consideres it to be 'int' only

class IntDemo{
	public static void main(String[] args){
		byte x = 8;
		byte y = 10;
		//byte z = x + y;
		int z = x + y;
		System.out.println(z);
	}
}

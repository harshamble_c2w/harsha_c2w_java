class DigitsInNumber{

	public static void main(String[] args){

		int num = 1234567;
		int temp = num;
		int digits = 0;
		while(temp>0){

			digits++;
			temp/=10;
		}

		System.out.println(digits);
	}
}

class StringArray{

	public static void main(String[] args){

		char chArr1[] = {'h','a','r','s','h'};
		System.out.println(chArr1);			//harsh
		System.out.println("chArr1: "+ chArr1);		//chArr1: harsh X	//chArr1: [C@:address
		String str1 = "harsh";
		System.out.println(str1+1);			//harsh1
		char chArr2[][] = {{'h'},{'a'},{'r'},{'s'},{'h'}};
		System.out.println(chArr2);			//[[C@:
		System.out.println(chArr2[0]);			//a
		System.out.println(chArr2[0]+"");		//[C@:
	}
}

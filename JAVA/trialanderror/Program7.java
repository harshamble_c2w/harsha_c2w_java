//  1  
// 121 
//12321 

class PatternDemo{

	public static void main(String[] args){

		int rows = 9;
		for(int i = 1; i<=rows; i++){
			int num = i;
			for(int j = 1; j<=rows-i; j++){
				System.out.print("  ");
			}
			for(int j = 1; j<=i; j++){
				System.out.print(j +" ");
			}
			for(int j = 1; j<i; j++){
				System.out.print(--num + " ");
			}
			System.out.println();
		}		
	}
}

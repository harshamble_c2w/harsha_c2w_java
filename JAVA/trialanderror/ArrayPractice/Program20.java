import java.util.Scanner;

class NumberOfTrailingZeros{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a number:");
		int num = sc.nextInt();

		int temp = num;
		int digits = 0;
		while(temp>0){
			
			digits++;
			temp/=10;
		}

		int numArr[] = new int[digits];
		temp = num;
		for(int i = 0; i<numArr.length; i++){

			int rem = temp%10;
			temp/=10;
			numArr[i] = rem;
		}

		int zeros = 0;
		for(int i = 0; i<numArr.length; i++){

			if(numArr[i] != 0)
				break;
			zeros++;
		}

		System.out.println("Number of trailing zeros: "+zeros);
	}
}

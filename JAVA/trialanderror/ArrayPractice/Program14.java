import java.util.Scanner;

class SumOfEvenOdd{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array:");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Start entering the elements:");
		for(int i = 0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}

		int evenSum = 0;
		int oddSum = 0;
		for(int i = 0; i<arr.length; i++){
			
			if(arr[i]%2==0)
				evenSum+=arr[i];
			else
				oddSum+=arr[i];
		}

		System.out.println("Sum of even elements: "+evenSum);
		System.out.println("Sum of odd elements: "+oddSum);
	}
}	

import java.util.Scanner;

class NoSpecialChars{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array:");
		int size = sc.nextInt();
		char arr[] = new char[size];

		for(int i = 0; i<arr.length; i++){

			arr[i] = sc.next().charAt(0);
		}

		for(int i = 0; i<arr.length; i++){

			if((arr[i]>=65 && arr[i]<=90) || (arr[i]>=95 && arr[i]<=122))
				System.out.print(arr[i]+" ");
		}

		System.out.println();
	}
}	

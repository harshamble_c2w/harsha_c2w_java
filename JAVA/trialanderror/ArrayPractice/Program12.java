import java.util.Scanner;

class CommonElements2Arrays{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array1:");
		int size1 = sc.nextInt();
		int arr1[] = new int[size1];

		System.out.println("Start entering the elements:");
		for(int i = 0; i<arr1.length; i++){

			arr1[i] = sc.nextInt();
		}

		System.out.println("Enter the size of array2:");
		int size2 = sc.nextInt();
		int arr2[] = new int[size2];

		System.out.println("Start entering the elements:");
		for(int i = 0; i<arr2.length; i++){

			arr2[i] = sc.nextInt();
		}

		int tempsize;
		if(size1>size2)
			tempsize = size2;
		else
			tempsize = size1;
		int temp[] = new int[tempsize];
		int cnt = 0;
		int commonCount = 0;

		for(int i = 0; i<arr1.length; i++){

			boolean found = false;
			for(int j = 0; j<arr2.length; j++){

				if(arr1[i] == arr2[j]){
					
					found = true;
					break;
				}
			}

			if(found == true){

				for(int j = 0; j<temp.length; j++){

					if(temp[j] == arr1[i])
						found = false;
				}
				if(found != false){

					commonCount++;
					temp[cnt++] = arr1[i];
				}
			}
		}

		if(commonCount == 0){

			System.out.println("There are no common elements!");
			return;
		}
		System.out.println("\nThere are "+commonCount+" common elements\nCommon elements are:");
		for(int i = 0; i<temp.length; i++){

			if(temp[i] == 0)
				break;
			System.out.println(temp[i]);
		}
	}
}	

import java.util.Scanner;

class MinElement{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array:");
		int size = sc.nextInt();
		int arr[] = new int[size];
	
		System.out.println("Start entering the elements:");
		for(int i = 0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}

		int minElement = arr[0];
		for(int i = 0; i<arr.length; i++){

			if(arr[i] < minElement)
				minElement = arr[i];
		}

		System.out.println("The minimum element from the array is: "+minElement);
	}
}

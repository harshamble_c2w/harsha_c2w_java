import java.util.Scanner;

class RemoveDuplicateNumbers{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Start entering the elements:");
		for(int i = 0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}

		int arrNew[] = new int[size];
		int cnt = 0;
		for(int i = 0; i<arr.length; i++){

			boolean visited = false;
			for(int j = 0; j<cnt; j++){

				if(arr[i] == arrNew[j]){
					
					visited = true;
					break;
				}
			}
			if(visited == false){

				arrNew[cnt++] = arr[i];
			}
		}

		if(cnt == 0){
			System.out.println("No duplicate elements");
			return;
		}

		System.out.println("The new array is:");
		for(int i = 0; i<cnt; i++){

			System.out.println(arrNew[i]);
		}
	}
}


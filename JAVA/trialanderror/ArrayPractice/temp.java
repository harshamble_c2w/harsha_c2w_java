class Demo{

	public static void main(String[] args){

		int a = 10000;
		int c = 10000;
		int b = 2140000000;
		char ch = 'a';

		System.out.println("a: "+System.identityHashCode(a));
		System.out.println("b: "+System.identityHashCode(b));
		System.out.println("c: "+System.identityHashCode(c));
		System.out.println("ch: "+System.identityHashCode(ch));
	}
}

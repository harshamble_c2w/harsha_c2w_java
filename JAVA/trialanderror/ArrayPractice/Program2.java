import java.util.Scanner;

class Input5{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		int arr[] = new int[5];

		System.out.println("Enter 5 elements for the array:");
		for(int i = 0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}
		
		for(int i = 0; i<arr.length; i++){

			System.out.println(arr[i]);
		}		
	}
}

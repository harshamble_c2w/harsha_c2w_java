import java.util.Scanner;

class Merge2Arrays{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array1:");
		int size1 = sc.nextInt();
		int arr1[] = new int[size1];

		System.out.println("Start entering the elements:");
		for(int i = 0; i<arr1.length; i++){

			arr1[i] = sc.nextInt();
		}

		System.out.println("Enter the size of array2:");
		int size2 = sc.nextInt();
		int arr2[] = new int[size2];

		System.out.println("Start entering the elements:");
		for(int i = 0; i<arr2.length; i++){

			arr2[i] = sc.nextInt();
		}

		int mergedSize = size1+size2;
		int mergedArr[] = new int[size1+size2];

		for(int i = 0; i<mergedArr.length; i++){
			
			if(i<arr1.length)
				mergedArr[i] = arr1[i];
			else
				mergedArr[i] = arr2[i-size1];
		}

		System.out.println("\nMerged array:");
		for(int i = 0; i<mergedArr.length; i++){

			System.out.println(mergedArr[i]);
		}
	}
}	

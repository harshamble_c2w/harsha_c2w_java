import java.util.Scanner;

class SumOfElements{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array:");
		int size = sc.nextInt();
		int arr[] = new int[size];
	
		System.out.println("Start entering the elements:");
		for(int i = 0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}

		int sum = 0;
		for(int i = 0; i<arr.length; i++){

			sum+=arr[i];
		}

		System.out.println("The sum of element in the array is: "+sum);
	}
}

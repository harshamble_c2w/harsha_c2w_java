import java.util.Scanner;

class ArmstrongNumberInArray{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size of array:");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Start entering the elements:");
		for(int i = 0; i<arr.length;i++){

			arr[i] = sc.nextInt();
		}

		System.out.println("\nThe armstrong elements are:");
		for(int i = 0; i<arr.length; i++){

			int temp = arr[i];
			int digits = 0;
			while(temp>0){

				digits++;
				temp/=10;
			}

			temp = arr[i];
			int sum = 0;
			while(temp>0){

				int rem = temp%10;
				temp/=10;
				int power = 1;
				for(int j = 0; j<digits; j++){
					
					power*=rem;
				}

				sum+=power;
			}

			if(sum == arr[i])
				System.out.println(arr[i]);
		}
	}
}

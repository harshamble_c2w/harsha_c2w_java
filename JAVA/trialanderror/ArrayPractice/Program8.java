import java.util.Scanner;

class FrequencyOfElements{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array:");
		int size = sc.nextInt();
		int arr[] = new int[size];
	
		System.out.println("Start entering the elements:");
		for(int i = 0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}

		int temp[] = new int[arr.length];
		for(int i = 0; i<arr.length; i++){

			temp[i] = arr[i];
		}

		for(int i = 0; i<arr.length; i++){

			int freq = 1;
			if(temp[i] == 2140000000)
				continue;
			for(int j = i+1; j<arr.length; j++){
	
				if(temp[i] == temp[j]){
				
					temp[j] = 2140000000;
					freq++;
				}
			}
			System.out.println("Frequency of "+arr[i]+" is "+freq);
		}
	}
}

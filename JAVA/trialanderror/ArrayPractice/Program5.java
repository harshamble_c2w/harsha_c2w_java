import java.util.Scanner;

class MaxElement{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array:");
		int size = sc.nextInt();
		int arr[] = new int[size];
	
		System.out.println("Start entering the elements:");
		for(int i = 0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}

		int maxElement = arr[0];
		for(int i = 0; i<arr.length; i++){

			if(arr[i]>maxElement)
				maxElement = arr[i];
		}

		System.out.println("The max element from the array is: "+maxElement);
	}
}

import java.util.Scanner;

class PairSumMatch{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array:");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Start entering the elements:");
		for(int i = 0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}

		System.out.println("Enter the sum you want to match:");
		int sum = sc.nextInt();

		for(int i = 0; i<arr.length; i++){

			for(int j = i+1; j<arr.length; j++){

				if((arr[i]+arr[j] == sum)){
					System.out.println("\nThe sum of elements at following indexes match the given sum value of "+sum+"\nindex: "+i+" index: "+j);
					return;
				}
			}
		}

		System.out.println("\nNo pair found in the array whose sum matches the given sum value");
	}
}

import java.util.Scanner;

class RemoveSpecificElement{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Start entering the elements:");
		for(int i = 0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}

		System.out.print("Enter element to remove: ");
		int key = sc.nextInt();

		//check if the element is present
		boolean found = false;
		int index = -1;	//to point to the index of first appearance of key elemeny
		for(int i = 0; i<arr.length; i++){

			if(arr[i] == key){
				found = true;
				index = i;
				break;
			}
		}

		//if element not found, print not found and stop program
		if(found == false){

			System.out.println("\nNo such element found");
			return;
		}
		
		//if element found, delete it and copy remaining elements in new array
		System.out.println("\nThe new array is:");
		int arrNew[] = new int[size-1];
		int cnt = 0;
		for(int i = 0; i<arr.length; i++){

			if(i == index)
				continue;
			arrNew[cnt++] = arr[i];
		}

		//to print new array
		for(int i = 0; i<arrNew.length; i++){

			System.out.println(arrNew[i]);
		}
	}
}

class StaticDemo{
	static int x = 10;
	int y = 20;
	static void fun(){
		StaticDemo sd = new StaticDemo();
		System.out.println(x);
		System.out.println(sd.y);
	}
	public static void main(String[] args){
		StaticDemo sd = new StaticDemo();
		fun();
	}
}

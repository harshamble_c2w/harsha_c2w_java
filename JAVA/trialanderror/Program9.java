import java.util.Scanner;

class PatternDemo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter number of rows: ");
		int rows = sc.nextInt();
		for(int i = 1; i<=rows; i++){

			int ch = 64+i;
			for(int j = 1; j<=rows+1-i; j++){

				if(rows%2==0){
					if(ch%2==0)
						System.out.print((char)(ch)+" ");
					else
						System.out.print(ch+" ");
				}
				else{
					if(ch%2==0)
						System.out.print(ch+" ");
					else
						System.out.print((char)(ch)+" ");
				}
				ch++;
			}
			System.out.println();
		}
	}
}

abstract class Baba{

	void getProperty(){

		System.out.println("Bunglow and Car");
	}

	//void toMarry();	//missing method body or declare abstract
	abstract void toMarry();
}

class Harsh extends Baba{

	void toMarry(){

		System.out.println("Kirti");
	}
}

class AbstractClassDemo{

	public static void main(String[] args){
		Baba obj = new Harsh();
		obj.getProperty();
		obj.toMarry();
	}
}

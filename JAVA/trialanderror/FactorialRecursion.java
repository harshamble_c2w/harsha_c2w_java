class FactorialRecursion{

	static void factorial(int n1, int n2){

		if(n2 >= 1000)
			return;
		System.out.print(n2+"\t");
		factorial(n2, n1+n2);
	}

	public static void main(String[] args){

		System.out.print("1\t");
		factorial(1,1);
	}

}

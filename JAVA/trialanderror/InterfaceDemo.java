interface InterfaceDemo{

	void getProperty();

	void career();
	
	void marry();
}

class Parent{

	public void fun(){

	}
}

class Demo extends Parent implements InterfaceDemo{


	public void fun(){

	}

	public void getProperty(){

		System.out.println("In getProperty");
	}

	public void career(){

		System.out.println("In career");
	}

	public void marry(){

		System.out.println("In marry");
	}

	void makingTea(){

		System.out.println("In tea");
	}

	public static void main(String[] args){

		Demo obj = new Demo();
		obj.getProperty();
		obj.career();
		obj.marry();
	}
}

class PrintArrayRecursion{

	void printArrayDemo(int array[], int cnt){

		if(cnt == array.length)
			return;
		System.out.println(array[cnt++]);
		printArrayDemo(array, cnt);
	}

	public static void main(String[] args){

		int arr[] = {1,2,3,4,5,6,7,8,9};
		PrintArrayRecursion par = new PrintArrayRecursion();
		par.printArrayDemo(arr, 0);
	}
}

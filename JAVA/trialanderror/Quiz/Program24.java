class Demo1{

	Demo1(){

		System.out.println("Demo1");
	}
}

class Demo2 extends Demo1{


}

class Demo3 extends Demo2{

	Demo3(){

		super();
		System.out.println("Demo3");
	}
}

class Demo4 extends Demo3{

	Demo4(){

		super();
	}
}

class Client{

	public static void main(String[] args){

		Demo4 obj = new Demo4();
	}
}

class Demo1{

	Demo1(){
		System.out.println("Demo1 - constructor");
	}
}

class Demo2 extends Demo1{

	Demo2(){

		System.out.println("DEmo2 - constructor");
	}
}

class Demo3 extends Demo2{

	Demo3(){

		System.out.println("Demo3");
	}
}

class Client{

	public static void main(String[] args){

		Demo1 obj = new Demo3();
	}
}

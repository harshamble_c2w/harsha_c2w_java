class PrintReverseArrayRecursion{

	void printArrayDemo(int array[], int cnt){

		if(cnt<0)
			return;
		System.out.println(array[cnt--]);
		printArrayDemo(array, cnt);
	}

	public static void main(String[] args){

		int arr[] = {1,2,3,4,5,6,7,8,9};
		PrintReverseArrayRecursion par = new PrintReverseArrayRecursion();
		par.printArrayDemo(arr, arr.length-1);
	}
}

class KPattern{

	public static void main(String[] args){

		int input = 7;
		int rows = input/2+1;
		for(int i = 1; i<=rows; i++){

			for(int j = 1; j<=rows+1-i; j++){
				System.out.print("* ");
			}
			System.out.println();
		}
		for(int i = 2; i<=rows; i++){

			for(int j = 1 ; j<=i; j++){

				System.out.print("* ");
			}
			System.out.println();
		}
	}
}

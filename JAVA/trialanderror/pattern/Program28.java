import java.util.Scanner;

class LeftPascalTriangle{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		int input = sc.nextInt();
		int rows;
		if(input%2==0)
			rows = input/2;
		else
			rows = input/2+1;
		for(int i = 1; i<=rows; i++){

			for(int j = 1; j<rows+1-i; j++){

				System.out.print("  ");
			}
			for(int j = 1; j<=i ; j++){

				System.out.print("* ");
			}
			System.out.println();
		}
		for(int i = 2; i<=rows; i++){

			for(int j = 1; j<i; j++){

				System.out.print("  ");
			}
			for(int j = 1; j<=rows+1-i; j++){

				System.out.print("* ");
			}
			System.out.println();
		}
	}
}

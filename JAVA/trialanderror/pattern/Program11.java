import java.util.*;

class DiamondPattern{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter number of rows: ");
		int rows = sc.nextInt();

		//for upper half
		for(int i = 1; i<=rows; i++){

			//for front spacing
			for(int j = 1; j<=rows-i; j++){

				System.out.print("  ");
			}

			for(int j = 1; j<=i; j++){

				System.out.print("*   ");
			}
			System.out.println();
		}

		//for lower half
		for(int i = 1; i<rows; i++){

			//for front spacing
			for(int j = 1; j<=i; j++){

				System.out.print("  ");
			}
			for(int j = 1; j<=rows-i; j++){

				System.out.print("*   ");
			}
			System.out.println();
		}
	}
}

class HollowHourGlassPattern{

	public static void main(String[] args){

		int rows = 10;
		for(int i = 1; i<rows; i++){

			for(int j = 1; j<=i; j++){

				System.out.print(" ");
			}
			for(int j = 1; j<=rows-i; j++){
				if(j==1 || j==rows-i || i==1)
					System.out.print("* ");
				else
					System.out.print("  ");
			}
			System.out.println();
		}

		for(int i = 2; i<=rows; i++){

			for(int j = 1; j<=rows-i; j++){

				System.out.print(" ");
			}
			for(int j = 1; j<=i; j++){
				if(j==1 || j==i || i==rows)
					System.out.print("* ");
				else
					System.out.print("  ");
			}
			System.out.println();
		}
	}
}

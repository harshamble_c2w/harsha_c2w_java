class PatternDemo{

	public static void main(String[] args){

		for(int i = 1; i<=4; i++){

			int num = 1;
			for(int j = 1; j<=4; j++){

				if(num<=i){

					System.out.print(num+" ");
					num++;
				}
				else
					System.out.print(" ");
			}
			System.out.println();
		}
	}
}

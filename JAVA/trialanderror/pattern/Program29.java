import java.util.*;

class InverseDiamondPattern{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter number of rows: ");
		int rows = sc.nextInt();
		int half;			//for distinguishing between upper and lower half
		if(rows%2==0)
			half = rows/2;
		else
			half = rows/2+1;
		for(int i = 1; i<=rows; i++){

			//upper half
			if(i<=half){
				if(rows%2==0 && i==half)		//to skip last row of upper half if rows are even
					continue;

				//leftmost triangle
				for(int j = 1; j<=half-i+1; j++){

					System.out.print("* ");
				}

				//spacing
				for(int j = 1; j<=2*(i-1)-1; j++){

					System.out.print("  ");
				}

				//rightmost triangle
				for(int j = 1; j<=half-i+1;j++){

					if(i==1 && j==half-i+1)
						continue;
					System.out.print("* ");
				}

				System.out.println();
			}

			//lower half
			else{

				//leftmost triangle
				for(int j = 1; j<=i-(rows-half); j++){

					System.out.print("* ");
				}

				//spacing
				for(int j = 1; j<=2*(rows-i)-1; j++){
		
					System.out.print("  ");
				} 

				//rightmost triangle
				for(int j = 1; j<=i-(rows-half); j++){

					if(i==rows && j==1)
						continue;
					System.out.print("* ");
				}
				System.out.println();
			}
		}
	}
}

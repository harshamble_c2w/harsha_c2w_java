class PalindromeTriangular{

	public static void main(String[] args){
		
		int rows = 7;
		for(int i = 1; i<=rows; i++){

			int num = i;
			for(int j = 1; j<=rows-i; j++){

				System.out.print("  ");
			}
			for(int j = 1; j<=i; j++){

				System.out.print(num-- +" ");
			}
			for(int j = 1; j<i; j++){

				System.out.print(j+1 +" ");
			}
			System.out.println();
		}		
	}
}

// *     *
// **   **
// *** ***
// *******
// *** ***
// **   **
// *     *

class ButterflyPattern{

	public static void main(String[] args){

		int input = 19;
		int rows = input/2+1;
		
		//for upper half
		for(int i = 1; i<rows; i++){

			for(int j = 1; j<=i; j++){
				
				System.out.print("* ");
			}
			for(int j = 1; j<=2*(rows-i)-1; j++){

				System.out.print("  ");
			}
			for(int j = 1; j<=i; j++){

				System.out.print("* ");
			}
			System.out.println();
		}

		if(input % 2 == 0){
			for(int i = 1; i<=input+1; i++){

				System.out.print("* ");
			}
		}
		else{
			for(int i = 1; i<=input; i++){
				System.out.print("* ");
			}
		}
		System.out.println();

		//for lower half
		for(int i = 1; i<rows; i++){

			for(int j = 1; j<=rows-i; j++){

				System.out.print("* ");
			}
			for(int j = 1; j<=2*i-1; j++){

				System.out.print("  ");
			}
			for(int j = 1; j<=rows-i; j++){

				System.out.print("* ");
			}
			System.out.println();
		}	
	}
}

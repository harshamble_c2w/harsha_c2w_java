class MirrorImageTriangePattern{

	public static void main(String[] args){

		int input = 10;
		int rows = input/2+1;
		for(int i = 1; i<=rows; i++){

			int num = i;
			for(int j = 1; j<i; j++){
				System.out.print(" ");
			}
			for(int j = 1; j<=rows+1-i; j++){

				System.out.print(num++ +" ");
			}
			System.out.println();
		}

		//for lower half
		for(int i = 1; i<=rows-1; i++){

			int num = rows - i;
			for(int j = 1; j<num; j++){

				System.out.print(" ");
			}
			for(int j = 1; j<=i+1; j++){

				System.out.print(num++ +" ");
			}
			System.out.println();
		}
	}
}

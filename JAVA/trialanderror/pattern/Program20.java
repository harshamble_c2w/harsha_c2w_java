class ReverseNumberTrianglePattern{

	public static void main(String[] args){

		int rows = 5;
		for(int i = 1; i<=rows; i++){

			int num = i;
			for(int j = 1; j<i; j++){

				System.out.print(" ");
			}
			for(int j = 1; j<=rows+1-i; j++){

				System.out.print(num++ +" ");
			}
			System.out.println();
		}
	}
}

class ReplaceStringElements{

	public static void main(String[] args){

		String str = "Atharva";
		char arr[] = new char[str.length()];
		for(int i = 0; i<str.length(); i++){

			arr[i] = str.charAt(i);
		}

		for(int i = 0; i<arr.length; i++){

			if(arr[i] == 'a' || arr[i] == 'A')
				arr[i] = 'z';
		}
		System.out.println(arr);
		str.replace('a','z');
		System.out.println(str);
		//System.out.println(str.replaceAll("ar","u"));
	}
}

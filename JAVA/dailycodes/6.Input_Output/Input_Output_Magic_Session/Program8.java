//take range as input from user and print the sum of numbers in the range

import java.io.*;

class IODemo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter starting number: ");
		int start = Integer.parseInt(br.readLine());
		System.out.print("Enter ending number: ");
		int end = Integer.parseInt(br.readLine());
		int sum = 0;
		for(int i = start; i<=end; i++)
			sum+=i;
		System.out.println("Sum: "+sum);
	}
}

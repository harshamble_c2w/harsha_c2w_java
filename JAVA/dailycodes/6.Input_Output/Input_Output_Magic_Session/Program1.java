//take number as input & check whether it is even or odd

import java.io.*;

class IODemo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter a number: ");
		int num = Integer.parseInt(br.readLine());
		if(num%2==0)
			System.out.println(num+" is even number");
		else
			System.out.println(num+" is odd number");
	}
}

//take number from user and check whether number is present in table of 16 or not

import java.io.*;

class IODemo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter a number: ");
		int num = Integer.parseInt(br.readLine());
		if(num%16==0)
			System.out.println(num+" is present in table of 16");
		else
			System.out.println(num+" is not present in table of 16");
	}
}

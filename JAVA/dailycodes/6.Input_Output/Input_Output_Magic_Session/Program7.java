//take range as input from user and print reverse table of it

import java.io.*;

class IODemo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter a number: ");
		int num = Integer.parseInt(br.readLine());
		for(int i = 1; i<= 10; i++)
			System.out.print(num*(11-i)+", ");
	}
}

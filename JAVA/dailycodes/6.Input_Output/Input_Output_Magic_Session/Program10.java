//take marks of 4 subjects from user and print the total marks obtained out of total. (100 marks each subject)

import java.io.*;

class IODemo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter marks out of 100 only");
		System.out.print("Enter marks for Maths: ");
		float math = Float.parseFloat(br.readLine());
		if(math<0 || math>100)
			return;
		System.out.print("Enter marks for English: ");
		float eng = Float.parseFloat(br.readLine());
		if(eng<0 || eng>100)
			return;
		System.out.print("Enter marks for Science: ");
		float sci = Float.parseFloat(br.readLine());
		if(sci<0 || sci>100)
			return;
		System.out.print("Enter marks for Marathi: ");
		float mar = Float.parseFloat(br.readLine());
		if(mar<0 || mar>100)
			return;
		float total = math+eng+sci+mar;
		System.out.println("Total Marks = "+total+" out of 400");

	}
}

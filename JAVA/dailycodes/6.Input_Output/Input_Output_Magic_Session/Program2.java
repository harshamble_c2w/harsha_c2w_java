import java.io.*;

class IODemo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter your age: ");
		int age = Integer.parseInt(br.readLine());
		if(age<0)
			System.out.println("Invalid age");
		else if(age<18)
			System.out.println("Voter is ineligible for voting");
		else
			System.out.println("Voter is eligible for voting");
	}
}

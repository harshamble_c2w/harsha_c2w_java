//we can reference the static variable 'y' using the class object because
//of the presence of the pointer to a special structure which is inside an
//object 
//the pointer points towards methods more specifically to the special 
//structre
//special structure hols all the static things' addresses

class StaticDemo{
	int x = 10;
	static int y = 20;
	public static void main(String[] args){
		StaticDemo obj = new StaticDemo();
		System.out.println(obj.x);
		System.out.println(obj.y);
	}
}

//error

class StaticDemo{
	int x = 10;
	static int y = 20;
	public static void main(String[] args){
		System.out.println(x);	//non static variable 'x' cannot be referenced from a static context
		System.out.println(y);
	}
}

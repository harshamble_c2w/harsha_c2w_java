//error

class StaticDemo{
	int x = 30;
	static int y = 40;

	void fun(){
		System.out.println("In fun");
	}
	static void run(){
		System.out.println("In run");
	}

	public static void main(String[] args){
		StaticDemo obj = new StaticDemo();
		System.out.println(x);	//non static variable 'x' cannot be referenced from a static context
		System.out.println(obj.y);
		fun();		//non static method 'fun' cannot be referenced from a static context
		obj.run();
	}
}

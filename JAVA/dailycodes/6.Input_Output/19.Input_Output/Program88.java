//error

class InputDemo{

	void methodFun(){
		System.out.println("In fun function");
	}

	void methodGun(){
		System.out.println("In gun function");
	}

	void methodRun(){
		System.out.println("In run function");
	}

	public static void main(String[] args){
		System.out.println("In main method");
		methodFun();	//non static method 'methodFun' cannot be referenced from a static context
		methodGun();	//non static method 'methodGun' cannot be referenced from a static context
		methodRun();	//non static method 'methodRun' cannot be referenced from a static context
	}
}

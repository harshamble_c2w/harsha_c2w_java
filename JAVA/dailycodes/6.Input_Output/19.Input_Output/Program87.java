//error

class InputDemo{

	void fun(){
		System.out.println("In fun function");
	}

	static void run(){
		System.out.println("In run function");
	}

	public static void main(String[] args){
		System.out.println("In main method");
		run();
		fun();		//non static method 'fun' cannot be referenced from a static context 
	}
}

import java.io.*;

class InputDemo{

	public static void main(String[] args){

		InputStreamReader isr = new InputStreamReader(System.in);
		int x = isr.read();		//read() throws IOException which must be caught or declared to be thrown
	}
}

import java.io.*;

class InputDemo{

	public static void main(String[] args)throws IOException{

		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		System.out.println("Enter company name: ");
		String cmpName = br.readLine();
		System.out.println("Enter employee name: ");
		String empName = br.readLine();
		System.out.println("Company name: "+cmpName);
		System.out.println("Employee name: "+empName);
	}
}

//error: incompatable types

import java.io.*;

class InputDemo{

	public static void main(String[] args)throws IOException{

		InputStreamReader isr = new InputStreamReader(System.in);
		char x = isr.read();		//possible lossy conversion
		System.out.println(x);
	}
}

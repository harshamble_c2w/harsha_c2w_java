//error

import java.io.*;

class InputDemo{

	public static void main(String[] args){

		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		String name = br.readLine();		//readLine() throws exception that must be caught or declared to be thrown
		System.out.println(name);
	}
}

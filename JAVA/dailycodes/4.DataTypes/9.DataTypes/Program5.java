//here there will be an error as the value 128 is out of the range of byte datatype
//incompatable types: possible lossy convergence from int to byte

class IntegerDemo{
	public static void main(String[] args){
		byte age = 128;
		System.out.println(age);
	}
}

//error as 32768 is out of range of short
//incompatible types: possible lossy conversion from int to short

class IntegerDemo{
	public static void main(String[] args){
		short x = 32768;
		System.out.println(x);
	}
}

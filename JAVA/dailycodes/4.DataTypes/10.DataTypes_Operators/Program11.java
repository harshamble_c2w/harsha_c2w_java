//error occurs as -129 is out of range of byte
//incompatible types: possible lossy conversion from int to byte

class IntegerDemo{
	public static void main(String[] args){
		byte age = -129;
		System.out.println(age);
	}
}

//no error as double can store float value
//since sizeof(float) < sizeof(double)

class FloatDemo{
	public static void main(String[] args){
		double balance = 100.50f;
		System.out.println(balance);
	}
}

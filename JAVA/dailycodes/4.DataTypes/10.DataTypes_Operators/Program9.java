//error will arise as 135 is out of range of byte
//incompatable types: possible lossy conversion from int to byte
//NOTE: java considers int as default datatye for numbers without decimals

class IntegerDemo{
	public static void main(String[] args){
		byte age = 135;
		System.out.println(age);
	}
}

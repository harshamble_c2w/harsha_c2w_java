//error
//

class SwitchDemo{

	public static void main(String[] args){

		float num = 1.5;	//1.5 is considered as double by default
		System.out.println("Before Switch");
		switch(num){		//float is not allowed in switch case
			case 1.5:
				System.out.println("1.5");
				break;
			case 2.0:
				System.out.println("2.0");
				break;
			case 2.5:
				System.out.println("2.5");
				break;
			default:
				System.out.println("In default state");
		}
		System.out.println("After Switch");
	}
}

class SwitchDemo{

	public static void main(String[] args){

		boolean x = true;
		switch(x){		//boolean not allowed in switch case

			case true:
				System.out.println("True");
				break;
			case false:
				System.out.println("False");
				break;
			default:
				System.out.println("Default");
		}
	}
}

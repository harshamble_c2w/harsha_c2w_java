//>>> is unsigned right shift
//it also shifts the sign bit
//which gets replaced by 0, so after performing >>> the answer is always positive

class BitwiseOperator{
	public static void main(String[] args){
		int x = -5;
		System.out.println(x>>>2);
	}
}

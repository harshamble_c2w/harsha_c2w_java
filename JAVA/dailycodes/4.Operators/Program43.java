// &,|,^,~,<<,>>,>>>

class BitwiseDemo{

	public static void main(String[] args){

		int x = 10;	//0000 1010
		int y = 15; 	//0000 1111
		System.out.println(x&y);	//0000 1010 => 10
		System.out.println(x|y);	//0000 1111 => 15
		System.out.println(x^y);	//0000 0101 => 5
		System.out.println(~y);		//1111 0000 => -16
	}
}

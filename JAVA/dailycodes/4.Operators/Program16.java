//for && if 1st operand is false, 2nd operand is not checked/considered as result will be false
//for || if 1st operand is true, 2nd operand is not checked/considered as result will be true

class LogicalCode{
	public static void main(String[] args){
		int x = 5;
		int y = 3;
		System.out.println((x-- <= ++y)&&(++x < y--));	//false
		System.out.println(x);				//4
		System.out.println(y);				//4
	}
}

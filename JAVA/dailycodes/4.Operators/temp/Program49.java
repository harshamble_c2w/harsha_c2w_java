class OperatorDemo{

	public static void main(String[] args){

		float a = 10;			//int can be stored in float as 10.0, no need to write f as it is taken as int by default
		double b = 10.5f;		//float value can be stored in double as its size is more and from same family
		System.out.println(a+b);	//20.5
		System.out.println(a);		//10.0
		System.out.println(b);		//10.5
	}
}

class OperatorDemo{

	public static void main(String[] args){

		int a = 10;
		System.out.println(a++ + ++a + a++ + ++a + a++);	//10+12+12+14+14 => 62
		System.out.println(a);		//15
	}
}

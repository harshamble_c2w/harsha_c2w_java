class OperatorDemo{

	public static void main(String[] args){

		float a = 10;		//converts int to float, considers 10 as int by default so no need to write f
		float b = 10.5f;
		System.out.println(a+b);
	}
}

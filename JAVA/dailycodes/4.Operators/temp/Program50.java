class OperatorDemo{

	public static void main(String[] args){

		float a = 10;
		double b = 10.5;
		float c = a + b;		//incompatable types: possible lossy conversion from double to float
		System.out.println(c);
		System.out.println(a);
		System.out.println(b);
	}
}

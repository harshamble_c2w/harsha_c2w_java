class OperatorDemo{

	public static void main(String[] args){

		int a = 10;
		int b = 15;
		int c = a++ + ++b + b++ + a++;		//10+16+16+11 => 53
		System.out.println(c);		//53
		System.out.println(b);		//12
		System.out.println(a);		//17
	}
}

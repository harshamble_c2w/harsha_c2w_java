class BitwiseOperator{
	public static void main(String[] args){
		int x = 10;
		System.out.println(~x);	//1111 1111 1111 1111 1111 1111 0101 => -11
		int z = 0b11110101;	//0000 0000 0000 0000 0000 0000 1111 0101
		System.out.println(z);	//245
	}
}

class BitwiseDemo{

	public static void main(String[] args){

		int x = 10;		//0000 1010
		int y = 15;		//0000 1111
		System.out.println(x<<3);	//0101 0000 => 80
		System.out.println(y>>2);	//0000 0011 => 3
	}
}

//1 B 3
//D 5 F
//7 H 9

class PatternDemo{

	public static void main(String[] args){

		int cntr = 1;
		for(int i = 1; i<=3; i++){
			for(int j=1; j<=3; j++){

				if(cntr%2 == 0)
					System.out.print((char)(64+cntr)+" ");
				else
					System.out.print(cntr+" ");
				cntr++;
			}
			System.out.println();
		}
	}
}

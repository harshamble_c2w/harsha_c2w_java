//1 2 3 4
//4 3 2 1
//1 2 3 4
//4 3 2 1

class PatternDemo{

	public static void main(String[] args){

		for(int i = 1; i<=4; i++){
			for(int j=1; j<=4; j++){

				if(i%2==0)
					System.out.print(5-j+" ");
				else
					System.out.print(j+" ");
			}
			System.out.println();
		}
	}
}

//1 8 9 64 25 216

class PatternDemo{

	public static void main(String[] args){

		for(int i=1; i<=6; i++){
			if(i%2 == 0)
				System.out.print(i*i*i+" ");
			else
				System.out.print(i*i+" ");
		}
	}
}

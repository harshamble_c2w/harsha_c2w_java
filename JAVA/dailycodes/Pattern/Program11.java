//A b C d E f G h I

class PatternDemo{

	public static void main(String[] args){

		for(int i = 1; i<=9; i++){
			if(i%2 == 0)
				System.out.print((char)(96+i)+" ");
			else
				System.out.print((char)(i+64)+" ");
		}
	}
}

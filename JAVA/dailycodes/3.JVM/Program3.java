//multiple classes with main methods are allowed but only 1 .class can be run at a time so only one main method will be used by JVM

class Core2Web{
	public static void main(String[] args){
		System.out.println("Core2Web");
	}
}

class Biencaps{
	public static void main(String[] args){
		System.out.println("Biencaps");
	}
}

class Incubators{
	public static void main(String[] args){
		System.out.println("Incubators");
	}
}

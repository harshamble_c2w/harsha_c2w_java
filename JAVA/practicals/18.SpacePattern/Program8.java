import java.util.Scanner;

class SpacePattern{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number of rows: ");
		int rows = sc.nextInt();
		/* LOGIC WITH 3 FOR LOOPS
		for(int i = 1; i<=rows; i++){

			int cnt = 1;
			for(int sp = 1; sp<i; sp++){
				System.out.print("  ");
				cnt++;
			}
			for(int j = 1; j<=rows+1-i; j++){
				System.out.print(cnt+" ");
				cnt++;
			}
			System.out.println();
		}*/
		// LOGIC WITH 2 FOR LOOPS
		for(int i = 1; i<=rows; i++){

			for(int j = 1; j<=rows; j++){
				if(j>=i)
					System.out.print(j+" ");
				else
					System.out.print("  ");
			}
			System.out.println();
		}
	}
}

import java.util.Scanner;

class SpacePattern{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number of rows: ");
		int rows = sc.nextInt();
		/* LOGIC WITH 3 FOR LOOPS
		for(int i = 1; i<=rows; i++){

			int cnt = 1;
			for(int sp = 1; sp<i; sp++){
				System.out.print("   ");
				cnt++;
			}
			for(int j = 1; j<=rows+1-i; j++){
				if(rows%2==0){
					if(cnt%2==0)
						System.out.print((char)(64+cnt)+"  ");
					else
						System.out.print(64+cnt+" ");
				}
				else{
					if(cnt%2==1)
						System.out.print((char)(64+cnt)+"  ");
					else
						System.out.print(64+cnt+" ");
				}
				cnt++;

			}
			System.out.println();
		}*/
		//LOGIC WITH 2 FOR LOOPS
		for(int i = 1; i<=rows; i++){

			for(int j = 1; j<=rows; j++){

				if(j>=i){
					if(rows%2==0){

						if(j%2==0)
							System.out.print((char)(64+j)+"  ");
						else
							System.out.print(64+j+" ");
					}
					else{

						if(j%2==1)
							System.out.print((char)(64+j)+"  ");
						else
							System.out.print(64+j+" ");
					}
				}
				else
					System.out.print("   ");
			}
			System.out.println();
		}
	}
}

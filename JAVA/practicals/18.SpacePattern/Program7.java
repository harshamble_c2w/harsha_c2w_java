import java.util.Scanner;

class SpacePattern{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number of rows: ");
		int rows = sc.nextInt();
		   for(int i = 1; i<=rows; i++){
			int num = 1;
			for(int sp = 1; sp<i; sp++){
				System.out.print("  ");
				num++;
			}
			for(int j = 1; j<=rows+1-i; j++){
				System.out.print(j+" ");
			}
			System.out.println();
		}
	}
}

//to print reverse of a number

import java.io.*;
class ReverseNumber{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter a number: ");
		int num = Integer.parseInt(br.readLine());
		int temp = num;
		int rem = 0;
		int var = 0;
		while(temp>0){

			rem = temp%10;
			var = var*10+rem;
			temp/=10;
		}
		System.out.println("Reverse of "+num+" is "+var);
	}
}

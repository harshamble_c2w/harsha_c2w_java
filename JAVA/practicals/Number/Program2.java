//to check whether a number is prime or not

import java.io.*;
class PrimeNumber{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter a number: "); 
		int num = Integer.parseInt(br.readLine());
		int temp = 1;
		int count = 0;
		while(temp<=num){

			if(num%temp==0)
				count++;
			temp++;
		}
		if(count==2)
			System.out.println(num+" is a prime number");
		else
			System.out.println(num+" is not a prime number");
	}
}

//to print the factors of a number

import java.io.*;
class FactorsDemo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter a number: ");
		int num = Integer.parseInt(br.readLine());
		int temp = 1;
		System.out.print("The factors of "+num+" are: ");
		while(temp<=num){

			if(num%temp==0)
				System.out.print(temp+",");
			temp++;
		}
		System.out.println();
	}
}

class SwitchDemo{

	public static void main(String[] args){

		int budget = 1000;

		switch(budget){

			case 2000:
				System.out.println("Destination: Mahabaleshwar");
				break;
			case 6000:
				System.out.println("Destination: Amritsar");
				break;
			case 10000:
				System.out.println("Destination: Manali");
				break;
			case 15000:
				System.out.println("Destination: Jammu and Kashmir");
				break;
			default:
				System.out.println("Try next time");
		}
	}
}

class IfElseDemo{

	public static void main(String[] args){

		int budget = 10000;

		if(budget >= 15000)
			System.out.println("Destination: Jammu and Kashmir");
		else if(budget >= 10000)
			System.out.println("Destination: Manali");
		else if(budget >= 6000)
			System.out.println("Destination: Amritsar");
		else if(budget >= 2000)
			System.out.println("Destination: Mahabaleshwar");
		else
			System.out.println("Try next time");
	}
}

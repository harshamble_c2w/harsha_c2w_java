class SwitchDemo{

	public static void main(String[] args){

		char grade = 'O';
		switch(grade){
			case 'O':
				System.out.println("Outstanding");
				break;
			case 'o':
				System.out.println("Outstanding");
				break;
			case 'A':
				System.out.println("Excellent");
				break;
			case 'a':
				System.out.println("Excellent");
				break;
			case 'B':
				System.out.println("Very Good");
				break;
			case 'b':
				System.out.println("Very Good");
				break;
			case 'C':
				System.out.println("Good");
				break;
			case 'c':
				System.out.println("Good");
				break;
			case 'P':
				System.out.println("Pass");
				break;
			case 'p':
				System.out.println("Pass");
				break;
			case 'F':
				System.out.println("Fail");
				break;
			case 'f':
				System.out.println("Fail");
				break;
			default:
				System.out.println("Invalid Input");
				break;
		}
	}
}

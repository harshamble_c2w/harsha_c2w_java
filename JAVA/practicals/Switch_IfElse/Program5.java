class SwitchDemo{

	public static void main(String[] args){

		char size = 'S';
		switch(size){
		
			case 'S':
				System.out.println("Small");
				break;
			case 's':
				System.out.println("Small");
			case 'M':
				System.out.println("Medium");
				break;
			case 'm': 
				System.out.println("Medium");
				break;
			case 'L':
				System.out.println("Large");
				break;
			case 'l':
				System.out.println("Large");
				break;
			default:
				System.out.println("Invalid size");
		}
	}
}

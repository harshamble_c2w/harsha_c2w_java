//rows = 3
//c
//C B
//c b a

//rows = 4
//d
//D C
//d c b
//D C B A

import java.io.*;
class PatternDemo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number of rows: ");
		int rows = Integer.parseInt(br.readLine());
		for(int i = 1; i<=rows; i++){

			for(int j = 1; j<=i; j++){

				if(i%2==0)
					System.out.print((char)(65+rows-j)+" ");
				else
					System.out.print((char)(97+rows-j)+" ");
			}
			System.out.println();
		}
	}
}

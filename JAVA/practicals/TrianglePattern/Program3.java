//rows = 3
//C
//C B 
//C B A

//rows = 4
//D
//D C
//D C B
//D C B A

import java.io.*;
class PatternDemo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number of rows: ");
		int rows = Integer.parseInt(br.readLine());
		for(int i = 1; i<=rows; i++){

			for(int j = 1; j<=i; j++){

				System.out.print((char)(65+rows-j)+" ");
			}
			System.out.println();
		}
	}
}

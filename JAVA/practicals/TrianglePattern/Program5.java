//rows = 3
//D
//E F
//G H I

//rows = 4
//E
//F G
//H I J
//K L M N

import java.io.*;
class PatternDemo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number of rows: ");
		int rows = Integer.parseInt(br.readLine());
		int cntr = 65+rows;
		for(int i = 1; i<=rows; i++){

			for(int j = 1; j<=i; j++){

				System.out.print((char)(cntr++)+" ");
			}
			System.out.println();
		}
	}
}

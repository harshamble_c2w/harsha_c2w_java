//rows = 3
//1
//B C
//1 2 3

//rows = 4
//1
//B C
//1 2 3
//G H I J

import java.io.*;
class PatternDemo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number of rows: ");
		int rows = Integer.parseInt(br.readLine());
		char ch = 65;
		for(int i = 1; i<=rows; i++){

			for(int j = 1; j<=i; j++){

				if(i%2==0)
					System.out.print(ch+" ");
				else
					System.out.print(j+" ");
				ch++;
			}
			System.out.println();
		}
	}
}

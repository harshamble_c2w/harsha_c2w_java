//rows = 3
//4
//4 a
//4 b 6

//rows = 4
//5
//5 a
//5 b 7
//5 c 7 d

import java.io.*;
class PatternDemo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number of rows: ");
		int rows = Integer.parseInt(br.readLine());
		char ch = 'a';
		for(int i = 1; i<=rows; i++){

			for(int j = 1; j<=i; j++){

				if(j%2==0)
					System.out.print(ch++ +" ");
				else
					System.out.print(rows+j+" ");
			}
			System.out.println();
		}
	}
}

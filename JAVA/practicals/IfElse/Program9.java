//check whether the given 3 numbers are pythagorean triples

class Pythagoras{

	public static void main(String[] args){

		int num1 = 6;
		int num2 = 10;
		int num3 = 8;

		if(num1*num1 + num2*num2 == num3*num3 || num2*num2 + num3*num3 == num1*num1 || num3*num3 + num1*num1 == num2*num2){
			System.out.println("Its pythagorean triplet");
		}
		else{
			System.out.println("Not a pythagorean truplet");
		}
	}
}

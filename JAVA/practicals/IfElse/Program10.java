//write a real time example

class Gear{

	public static void main(String[] args){

		int speed = 100;

		if(speed < 0){
			System.out.println("Invalid input for speed");
		}
		else if(speed <= 15){
			System.out.println("1st gear");
		}
		else if(speed <= 25){
			System.out.println("2nd gear");
		}
		else if(speed <= 45){
			System.out.println("3rd gear");
		}
		else if(speed <= 65){
			System.out.println("4th gear");
		}
		else{
			System.out.println("5th gear");
		}
	}
}

//check character is vowel or consonant

class Vowel{

	public static void main(String[] args){

		char ch = 20;

		if((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z')){

			if(ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u' || ch == 'A' || ch == 'E' || ch == 'I' || ch == 'O' || ch == 'U'){
				System.out.println(ch+" is a vowel");
			}
			else{
				System.out.println(ch+" is a consonant");
			}
		}
		else{

			System.out.println("Invalid input, please enter a letter");
		}
	}
}

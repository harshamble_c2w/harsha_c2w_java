//check age of voter
//if <0 print "Invalid age"
//if >18 print "Valid age for voitng"
//if >0 & <18 print "Valid age but cannot vote yet"

class VoterAge{

	public static void main(String[] args){

		int age = 25;

		if(age < 0){
			System.out.println("Invalid age");
		}
		else if(age < 18){
			System.out.println("Valid age but cannot vote");
		}
		else{
			System.out.println("Valid age for voting");
		}
	}
}

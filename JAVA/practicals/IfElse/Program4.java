//suggest career field based on student's percentage
//>85 : suggest medical
//<=85 & >75 : suggest engineering
//<=75 & >65 : suggest pharmacy or bachelor in science
//<=65 : family business (if any)

class Career{

	public static void main(String[] args){

		float percentage = 37.65f;

		if(percentage > 85){
			System.out.println("Medical");
		}
		else if(percentage <= 85 && percentage > 75){
			System.out.println("Engineering");
		}
		else if(percentage <= 75 && percentage > 65){
			System.out.println("Pharmacy or Bachelor in Science");
		}
		else{
			System.out.println("Family business (if any)");
		}
	}
}

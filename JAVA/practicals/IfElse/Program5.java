//given float value is divisible by 6 or not


class Divisible{

	public static void main(String[] args){

		float num = 51.6f;

		if(num % 6.0 == 0){
			System.out.println(num+" is divisible by 6");
		}
		else{
			System.out.println(num+" is not divisible by 6");
		}
	}
}

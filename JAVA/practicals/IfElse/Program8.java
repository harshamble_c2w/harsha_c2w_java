//check grade

class Grade{

	public static void main(String[] args){

		float percentage = 40;

		if(percentage < 0 || percentage >100){
			System.out.println("Invalid percentage!!");
		}
		else if(percentage >= 75){
			System.out.println("First class with distinction");
		}
		else if(percentage >= 60){
			System.out.println("First class");
		}
		else if(percentage >= 50){
		       System.out.println("Second class");
		}
 		else if(percentage >= 40){
			System.out.println("Pass");
		}
		else{
			System.out.println("Fail");
		}
	}
}

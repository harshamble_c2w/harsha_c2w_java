//calculate profit or loss

class Profit_Loss{

	public static void main(String[] args){

		int cost_price = 9000;
		int selling_price = 8000;
		int pl = selling_price - cost_price;
		
		if(pl > 0){
			System.out.println("Profit of "+pl);
		}
		else if(pl < 0){
			System.out.println("Loss of "+pl);
		}
		else{
			System.out.println("No profit no loss");
		}
	}
}

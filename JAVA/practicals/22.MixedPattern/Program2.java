import java.util.*;

class MixedPattern{

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number of rows : ");
		int rows = sc.nextInt();
		int num = 1;
		for(int i = 1; i<=rows; i++){
			for(int sp = 1; sp<=i-1; sp++){
				System.out.print("  ");
			}

			for(int j = 1; j<=rows-i+1; j++){
				System.out.print(num++ +" ");		
			}
			num--;
			System.out.println();
		}
	}
}

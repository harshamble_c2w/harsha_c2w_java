import java.util.Scanner;

class MixedPattern{

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number of rows : ");
		int rows = sc.nextInt();
		int num = -1;
		for(int i = 1; i<=rows; i++){

			for(int sp = 1; sp<=rows-i; sp++){
				
				System.out.print("   ");
			}

			for(int j = 1 ; j<=i; j++){
			
				if(i%2==0){
		
					if(j%2==1)
						System.out.print((num+=2)+"  ");
					else
						System.out.print((num+=i)+"  ");
				}
				else
					System.out.print((num+=2)+"  ");
			}
			System.out.println();
		}
	}
}

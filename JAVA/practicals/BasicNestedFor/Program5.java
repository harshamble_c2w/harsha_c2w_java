//no of rows = 3
//1A 1A 1A
//1A 1A 1A
//1A 1A 1A

//no of rows = 4
//1A 1A 1A 1A
//1A 1A 1A 1A
//1A 1A 1A 1A
//1A 1A 1A 1A

class P5{

	public static void main(String[] args){

		int rows = 4;
		for(int i=1; i<=rows; i++){
			for(int j=1; j<=rows; j++){
				System.out.print("1A ");
			}	
			System.out.println();
		}
	}
}

//no of rows = 3
//c b a
//c b a
//c b a

//no of rows = 4
//d c b a
//d c b a
//d c b a
//d c b a

class P7{

	public static void main(String[] args){

		int rows = 4;
		for(int i=1; i<=rows; i++){
			int num = rows;
			for(int j=1; j<=rows; j++){
				System.out.print((char)(96+num)+" ");
				num--;
			}	
			System.out.println();
		}
	}
}

//no of rows = 3
//1 2 3
//2 3 4
//3 4 5

//no of rows = 4
//1 2 3 4
//2 3 4 5
//3 4 5 6
//4 5 6 7

class P10{

	public static void main(String[] args){

		int rows = 4;
		for(int i=1; i<=rows; i++){
			int num = i;
			for(int j=1; j<=rows; j++){
				System.out.print(num++ +" ");
			}	
			System.out.println();
		}
	}
}

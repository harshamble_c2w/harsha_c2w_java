//no of rows = 3
//1 1 1
//2 2 2
//3 3 3

//no of rows = 4
//1 1 1 1
//2 2 2 2
//3 3 3 3
//4 4 4 4

class P3{

	public static void main(String[] args){

		int rows = 4;
		for(int i=1; i<=rows; i++){
			for(int j=1; j<=rows; j++){
				System.out.print(i+" ");
			}	
			System.out.println();
		}
	}
}

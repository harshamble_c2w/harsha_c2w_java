//no of rows = 3
//1A 2B 3C
//1A 2B 3C
//1A 2B 3C

//no of rows = 4
//1A 2B 3C 4D
//1A 2B 3C 4D
//1A 2B 3C 4D
//1A 2B 3C 4D

class P6{

	public static void main(String[] args){

		int rows = 4;
		for(int i=1; i<=rows; i++){
			for(int j=1; j<=rows; j++){
				System.out.print(j+""+(char)(j+64)+" ");
			}	
			System.out.println();
		}
	}
}

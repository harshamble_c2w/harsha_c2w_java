//no of rows = 3
//1 2 3
//3 4 5
//5 6 7

//no of rows = 4
//1 2 3 4
//4 5 6 7
//7 8 9 10
//10 11 12 13

class P9{

	public static void main(String[] args){

		int num = 1;
		int rows = 4;
		for(int i=1; i<=rows; i++){
			for(int j=1; j<=rows; j++){
				System.out.print(num++ +" ");
			}	
			System.out.println();
			num--;
		}
	}
}

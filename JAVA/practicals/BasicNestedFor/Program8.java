//no of rows = 3
//C1 C2 C3
//C4 C5 C6
//C7 C8 C9

//no of rows = 4
//D1 D2 D3 D4
//D5 D6 D7 D8
//D9 D10 D11 D12
//D13 D14 D15 D16

class P8{

	public static void main(String[] args){

		int num = 1;
		int rows = 4;
		for(int i=1; i<=rows; i++){
			for(int j=1; j<=rows; j++){
				System.out.print((char)(64+rows)+""+num++ +" ");
			}	
			System.out.println();
		}
	}
}

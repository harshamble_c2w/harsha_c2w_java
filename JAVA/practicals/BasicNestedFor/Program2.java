//no of rows = 3
//1 2 3
//1 2 3
//1 2 3

//no of rows = 4
//1 2 3 4
//1 2 3 4
//1 2 3 4
//1 2 3 4

class P2{

	public static void main(String[] args){

		int rows = 4;
		for(int i=1; i<=rows; i++){
			for(int j=1; j<=rows; j++){
				System.out.print(j+" ");
			}	
			System.out.println();
		}
	}
}

//error : unclosed character literal
//char only takes 1 character as input ('9')
//but when we do char = 95, it takes 95 as the ASCII code

class C2W_CharDemo{
	public static void main(String[] args){
		char ch = '97';
		System.out.println("char = "+ch);
		System.out.println("char = "+ch+1);
	}
}

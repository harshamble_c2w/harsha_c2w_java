class C2W_FloatDemo{
	public static void main(String[] args){
		double num1 = 90.89D;
		System.out.println(num1);
		float num2 = num1;	//error here 
					//incompatable types: possible lossy conversion from double to float
					//sizeof(double) > sizeof(float)
		System.out.println(num2);
	}
}

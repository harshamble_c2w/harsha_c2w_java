//error: incommpatible types: possible lossy conversion from int to char
//sizeof(char) < sizeof(int)
//so char ch1 = ch; will give the error

class Core2Web{
	public static void main(String[] args){
		int ch = 65; 
		char ch1 = ch; 
		System.out.println(ch);
		System.out.println(ch1);
	}
}

//no error

class C2W_ShortDemo{
	public static void main(String[] args){
		short sh1 = 'A';	//it stores the ASCII code of A, i.e., 65
		short sh2 = '1';	//it stores the ASCII code of 1, i.e., 49
		System.out.println(sh1);
		System.out.println(sh2);
	}
}

class C2W_LongDemo{
	public static void main(String[] args){
		long num1 = 2000005;
		int num2 = num1l;	//error here
					//cannot find symbol 'num1l'
					//we can only give 'l', 'f' to values only and not to vairable name
		System.out.println(num1);
	}
}

//no error as sizeof(int) > sizeof(short)

class C2W_ShortDemo{
	public static void main(String[] args){
		short sh1 = 19;
		short sh2 = 11;
		int sh3 = sh1+sh2;
		short sh4 = sh1+sh2; //error
				     //incompatable types: possible lossy conversino from int to short
				     //because 19 and 11 are stored as int even if we give short data type
				     //JAVA considers any non decimal number as int
				     //to rectify the error, we need to do explicit typecasting
		System.out.println(sh3);
	}
}

//no error

class C2W_FloatDemo{
	public static void main(String[] args){
		int num1 = 959697;
		float num2 = 959697.12345f;	//accepts the value but rounds it off to 1 decimal
		double num3 = num2;		//accepts the value but rounds it off to 3 decimal
		System.out.println(num1);
		System.out.println(num2);
		System.out.println(num3);
	}
}

import java.util.Scanner;

class DiamonPattern{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int rows = sc.nextInt();
		int space;
		int col = 0;
		int num;
		for(int i = 1; i<rows*2; i++){

			if(i<=rows){

				space = rows - i;
				col = 2*i-1;
				num = rows-i+1;
			}
			else{

				space = i - rows;
				col -= 2;
				num = i-rows+1;
			}

			for(int sp = 1; sp<=space; sp++){

				System.out.print("\t");
			}

			for(int j = 1; j<=col; j++){
	
				System.out.print(num +"\t");
			}
			System.out.println();
		}
	}
}

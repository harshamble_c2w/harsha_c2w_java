import java.util.Scanner;

class DiamonPattern{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int rows = sc.nextInt();
		int space;
		int col = 0;
		for(int i = 1; i<rows*2; i++){

			int cnt = rows;
			if(i<=rows){

				space = rows - i;
				col = 2*i-1;
			}
			else{

				space = i - rows;
				col -= 2;
			}

			for(int sp = 1; sp<=space; sp++){

				System.out.print("\t");
				cnt--;
			}

			for(int j = 1; j<=col; j++){

				if(j<=col/2)
					System.out.print(cnt-- +"\t");
				else
					System.out.print(cnt++ +"\t");
			}
			System.out.println();
		}
	}
}

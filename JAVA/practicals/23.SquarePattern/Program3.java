import java.util.Scanner;

class SquarePattern{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number of rows:");
		int rows = sc.nextInt();
		int cnt = rows;
		for(int i = 1; i<=rows; i++){

			for(int j = 1; j<=rows; j++){

				if(i%2==1){
					if(j%2==1)
						System.out.print((char)(96+cnt++)+" ");
					else
						System.out.print(cnt++ +" ");
				}
				else{
					if(j%2==0)
						System.out.print((char)(96+cnt++)+" ");
					else
						System.out.print(cnt++ +" ");
				}
			}
			System.out.println();
		}
	}
}

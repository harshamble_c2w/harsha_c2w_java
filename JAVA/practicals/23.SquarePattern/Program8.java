import java.util.Scanner;

class SquarePattern{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number of rows:");
		int rows = sc.nextInt();
		int cnt = rows;
		for(int i = 1; i<=rows; i++){

			for(int j = 1; j<=rows; j++){

				if(i%2==1 && j%2==1)
					System.out.print(cnt*cnt-1+" ");
				else 
					System.out.print((char)(97+cnt) +" ");
				cnt++;
			}
			System.out.println();
		}
	}
}

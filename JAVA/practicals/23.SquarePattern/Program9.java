import java.util.Scanner;

class SquarePattern{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number of rows:");
		int rows = sc.nextInt();
		int cnt = rows*rows;
		for(int i = 1; i<=rows; i++){

			for(int j = 1; j<=rows; j++){

				if(j%2==0)
					System.out.print("@ ");
				else
					System.out.print(cnt+" ");
				cnt-=i;
			}
			cnt+=rows*(rows-i);
			System.out.println();
		}
	}
}

import java.util.Scanner;

class SquarePattern{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number of rows:");
		int rows = sc.nextInt();
		for(int i = 1; i<=rows; i++){

			int num = rows*(rows+1-i);
			int cnt = num;
			for(int j = 1; j<=rows; j++){

				if(i==j)
					System.out.print("$ ");
				else
					System.out.print(num+" ");
				cnt-=2;
				num+=cnt;

			}
			System.out.println();
		}
	}
}

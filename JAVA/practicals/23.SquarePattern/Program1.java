import java.util.Scanner;

class SquarePattern{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number of rows:");
		int rows = sc.nextInt();
		int cnt = rows;
		for(int i = 1; i<=rows; i++){

			for(int j = 1; j<=rows; j++){

				if(j==1)
					System.out.print((char)(64+cnt++)+" ");
				else
					System.out.print((char)(96+cnt++)+" ");
			}
			System.out.println();
		}
	}
}

import java.util.Scanner;

class SquarePattern{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number of rows:");
		int rows = sc.nextInt();
		for(int i = 1; i<=rows; i++){

			int cnt = rows*rows;
			for(int j = 1; j<=rows; j++){

				if(i%2==1)
					System.out.print(cnt--+" ");
				else if(j%2==1)
					System.out.print(cnt+" ");
				else
					System.out.print((cnt-=5)+" ");
			}
			System.out.println();
		}
	}
}

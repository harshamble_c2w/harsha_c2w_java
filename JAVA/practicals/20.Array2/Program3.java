//check if there is any vowel in array of chars
//if present, print its index

import java.util.Scanner;
class Vowels{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size of the array: ");
		int size = sc.nextInt();
		char arr[] = new char[size];
		System.out.println("Start entering the elements: ");
		String str = sc.next();
		for(int i = 0; i<size; i++){
			arr[i] = str.charAt(i);
		}
		for(int i = 0; i<arr.length; i++){
			if(arr[i]=='a' || arr[i]=='e' || arr[i]=='i' || arr[i]=='o' || arr[i]=='u' || arr[i]=='A' || arr[i]=='E' || arr[i]=='I' || arr[i]=='O' || arr[i]=='U')
				System.out.println("vowel "+arr[i]+" found at index "+i);
		}
	}
}

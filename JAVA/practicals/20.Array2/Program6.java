//product of odd indexed elements

import java.util.Scanner;
class ProductOddIndex{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size of the array: ");
		int size = sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Start entering the elements: ");
		for(int i = 0; i<size; i++){
			arr[i] = sc.nextInt();
		}
		int productOddIndex = 1;
		for(int i = 0; i<arr.length; i++){
			if(i%2==1)
				productOddIndex*=arr[i];
		}
		System.out.println("Product of odd indexed elements: "+productOddIndex);
	}
}

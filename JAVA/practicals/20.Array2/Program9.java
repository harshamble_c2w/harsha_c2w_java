//print minimum element in array

import java.util.Scanner;
class MinElement{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size of the array: ");
		int size = sc.nextInt();
		if(size <= 0){
			System.out.println("Invalid array size");
			return;
		}
		int arr[] = new int[size];
		System.out.println("Start entering the elements: ");
		for(int i = 0; i<size; i++){
			arr[i] = sc.nextInt();
		}
		int minElement = arr[0];
		for(int i = 0; i<arr.length; i++){
			if(arr[i]<minElement)
				minElement = arr[i];
		}
		System.out.println("Minimum number in array is: "+minElement); 
	}
}

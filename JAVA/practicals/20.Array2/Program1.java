//count even numbers in the array and print the even numbers too

import java.util.Scanner;
class ArrayDemo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size of the array: ");
		int size = sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Start entering the elements: ");
		for(int i = 0; i<size; i++){
			arr[i] = sc.nextInt();
		}
		int evenCount = 0;
		System.out.print("Even numbers: ");
		for(int i = 0; i<arr.length; i++){
			if(arr[i]%2==0){
				evenCount++;
				System.out.print(arr[i]+" ");
			}
		}
		System.out.println("\nCount of even numbers: "+evenCount); 
	}
}

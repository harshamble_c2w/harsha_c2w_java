//print array
//if size of array is even, print alterante elements
//else print whole array

import java.util.Scanner;
class PrintArray{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size of the array: ");
		int size = sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Start entering the elements: ");
		for(int i = 0; i<size; i++){
			arr[i] = sc.nextInt();
		}
		System.out.println("Array elements are:");
		if(arr.length%2==0){
			for(int i = 0; i<arr.length; i+=2){
				System.out.println(arr[i]+" ");
			}
		}
		else{
			for(int i = 0; i<arr.length; i++){
				System.out.println(arr[i]+" ");
			}
		}
	}
}

//print sum of odd indexed elements

import java.util.Scanner;
class SumOddIndex{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size of the array: ");
		int size = sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Start entering the elements: ");
		for(int i = 0; i<size; i++){
			arr[i] = sc.nextInt();
		}
		int sumOddIndex = 0;
		for(int i = 0; i<arr.length; i++){
			if(i%2==1)
				sumOddIndex+=arr[i];
		}
		System.out.println("Sum of odd indexed elements: "+sumOddIndex); 
	}
}

//searching a specific element in array and print its index

import java.util.Scanner;
class SearchNum{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size of the array: ");
		int size = sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Start entering the elements: ");
		for(int i = 0; i<size; i++){
			arr[i] = sc.nextInt();
		}
		System.out.println("Enter the number to search in array:");
		int num = sc.nextInt();
		int found = 0;
		for(int i = 0; i<arr.length; i++){
			if(arr[i]==num){
				System.out.println(arr[i]+" found at index "+i);
				found = 1;
			}
		}
		if(found==0)
			System.out.println("Number not found");
	}
}

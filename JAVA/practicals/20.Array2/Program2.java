//print sum of elements divisible by 3

import java.util.Scanner;
class DivBy3{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size of the array: ");
		int size = sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Start entering the elements: ");
		for(int i = 0; i<size; i++){
			arr[i] = sc.nextInt();
		}
		int sum = 0;
		System.out.print("Elements divisible by 3: ");
		for(int i = 0; i<arr.length; i++){
			if(arr[i]%3==0){
				sum+=arr[i];
				System.out.print(arr[i]+" ");
			}
		}
		System.out.println("\nSum of elements divisible by 3: "+sum); 
	}
}

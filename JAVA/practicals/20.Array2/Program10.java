//print maximum element in array

import java.util.Scanner;
class MaxElement{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size of the array: ");
		int size = sc.nextInt();
		if(size <= 0){
			System.out.println("Invalid array size");
			return;
		}
		int arr[] = new int[size];
		System.out.println("Start entering the elements: ");
		for(int i = 0; i<size; i++){
			arr[i] = sc.nextInt();
		}
		int maxElement = arr[0];
		int position = 0;
		for(int i = 0; i<arr.length; i++){
			if(arr[i]>maxElement){
				maxElement = arr[i];
				position = i;
			}
		}
		System.out.println("Maximum number in array is found at position "+position+" is: "+maxElement);
	}
}

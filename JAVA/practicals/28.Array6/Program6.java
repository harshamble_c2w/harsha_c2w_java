import java.util.*;

class ElementMultiple{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Size for array: ");
		int size = sc.nextInt();
		int arr1 [] = new int[size];

		System.out.println("\n\n\nEnter Array one elemnts:");	
		for(int i=0;i<arr1.length;i++){
			System.out.print("\nEnter element: ");
			arr1[i]=sc.nextInt();
		}

		System.out.println("\nEnter the key: ");
		int key = sc.nextInt();

		boolean is_present = false;
		for(int i = 0; i<arr1.length; i++){

			if(arr1[i]%key==0){
				is_present=true;
				System.out.println("\nAn element multilple of "+key+" found at index : "+i);
			}
			
		}

		if(!is_present)
			System.out.print("\nKey not present");

		System.out.println();
	}
}

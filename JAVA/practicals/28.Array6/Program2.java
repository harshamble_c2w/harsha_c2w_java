import java.util.*;

class SumAndCountOfPrime{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Size : ");
		int size = sc.nextInt();
		int arr [] = new int[size];
		for(int i=0;i<arr.length;i++){
			System.out.print("\nEnter element : ");
			arr[i]=sc.nextInt();
		}

		int sum = 0;
		int cnt_prime = 0;
		for(int i = 0; i<arr.length; i++){
			boolean is_prime = true;
			int num = (int)Math.sqrt(arr[i]);
			for(int j = 2; j<=num; j++){
				if(arr[i]%j==0){
					
					is_prime=false;
					break;
				}
			}
			if(is_prime){
				//System.out.println("\n arr "+arr[i]);
				sum+=arr[i];
				cnt_prime++;
			}	

		}
		System.out.println("\nSum of all prime number is: "+sum+"\nCount of prime numbers: "+cnt_prime);
	}
}

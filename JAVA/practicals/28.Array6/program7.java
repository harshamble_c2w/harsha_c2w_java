import java.util.*;

class InsertBasedOnASCII{

	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Size for array: ");
		int size = sc.nextInt();
		String arr[] = new String[size];
	
		System.out.println("\n\n\nEnter Array one elemnts :");	
		for(int i = 0; i<arr.length; i++){

			System.out.print("\nEnter element : ");
			int num = sc.nextInt();
			if(num>=64 && num<=90)
				arr[i]=(char)num;
			else
				arr[i]=num;
		}

		for(int i = 0; i<arr.length; i++){
			
			System.out.print(arr[i]+" ");
		}
	
		System.out.println();
	}
}

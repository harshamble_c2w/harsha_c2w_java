import java.util.*;

class CombineArrays{

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Size for array 1 : ");
		int size = sc.nextInt();
		int arr1 [] = new int[size];

		System.out.println("\nEnter Array one elemnts :");	
		for(int i=0;i<arr1.length;i++){
			System.out.print("\nEnter element : ");
			arr1[i]=sc.nextInt();
		}


		System.out.println("\n\nEnter Size for array 2 : ");
		int size2 =sc.nextInt();
		int arr2[] = new int[size2];

		System.out.println("\nEnter Array two elemnts :");	
		for(int i = 0; i<arr2.length; i++){
			System.out.print("\nEnter element : ");
			arr2[i]=sc.nextInt();
		}

		int arr3[] = new int [arr1.length+arr2.length];

		System.out.print("\nElements of combined array are : ");
		for(int i = 0; i<arr3.length; i++){

			if(i<arr1.length)
				arr3[i] = arr1[i];
			else
				arr3[i] = arr2[i-arr1.length];
			System.out.print(arr3[i]+" ");
			
		}


		System.out.println();
	}
}

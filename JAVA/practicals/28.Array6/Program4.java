import java.util.*;

class CommonElements{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Size : ");
		int size = sc.nextInt();
		int arr1[] = new int[size];
		int arr2[] = new int[size];

		System.out.println("\n\n\nEnter Array one elemnts :");	
		for(int i=0;i<arr1.length;i++){
			System.out.print("\nEnter element : ");
			arr1[i]=sc.nextInt();
		}


		System.out.println("\n\nEnter Array two elemnts :");	
		for(int i=0;i<arr2.length;i++){
			System.out.print("\nEnter element : ");
			arr2[i]=sc.nextInt();
		}

		int visited[] = new int[size];
		int cntr = 0;
		System.out.print("\nCommon elements are : ");
		for(int i = 0; i<arr1.length; i++){

			boolean did_visit = false;
			for(int j = 0; j<cntr; j++){
		
				if(arr1[i]==visited[j])
					did_visit = true;
			}
			
			if(!did_visit)
				visited[cntr++] = arr1[i];
			else
				continue;

			for(int j = 0; j<arr2.length; j++){
				
				if(arr1[i]==arr2[j]){	
				
					System.out.print(arr1[i]+" , ");
					break;
				}
			}
			
		}


		System.out.println();
	}
}

import java.util.*;

class CountMoreThan2{

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Size : ");
		int size = sc.nextInt();
		int arr [] = new int[size];
	
		System.out.println("Start entering the elements:");
		for(int i=0;i<arr.length;i++){
			
			arr[i]=sc.nextInt();
		}

		System.out.println("\nEnter the key :");
		int key = sc.nextInt();
		int cnt = 0;
		boolean key_pre = false;
		for(int i = 0;i<arr.length;i++){
			if(key==arr[i]){	
				key_pre=true;
				cnt++;
			}	
		}

		if(!key_pre){

			System.out.println(key+" is not present in the given array!");
		}
		else if(cnt<3){

			System.out.println(key+" is not present more than 2 times, so no change in given array!");
		}
		else{
			
			System.out.println("Array after changes:");
			for(int i = 0; i<arr.length; i++){

				if(arr[i]==key)
					arr[i]=arr[i]*arr[i]*arr[i];
				System.out.print(arr[i]+" ");
			}
			System.out.println();
		}
	}
}

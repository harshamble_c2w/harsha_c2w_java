import java.util.*;

class DescendingOrder{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Size : ");
		int size = sc.nextInt();
		int arr [] = new int[size];

		for(int i=0;i<arr.length;i++){
			System.out.print("\nEnter element : ");
			arr[i]=sc.nextInt();
		}

		boolean is_descending = true;
		for(int i = 1; i<arr.length; i++){
			
			if(arr[i-1]<arr[i]){
			
				is_descending=false;
				break;
			}

		}

		if(is_descending)
			System.out.println("\nThe given array is in descending order.");
		else
			System.out.println("\nthe gigven  array is not in desscending order");
	}
}

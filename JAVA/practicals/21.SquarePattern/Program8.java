import java.util.Scanner;

class SquarePattern{

	public static void main(String[]args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number of Rows : ");
		int rows = sc.nextInt();
		int num = 1;
		for(int i = 1; i<=rows; i++){

			int ch = rows;
			for(int j = 1; j<=rows; j++){

				if(num%2==0)
					System.out.print((char)(64+ch--) + " ");
				else 
					System.out.print("# ");
				num++;
			}
			if(rows%2==0)
				num--;
			System.out.println();
		}
	}		
}

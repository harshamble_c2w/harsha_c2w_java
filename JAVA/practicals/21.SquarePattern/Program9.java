import java.util.*;
class SquarePattern{
	public static void main(String[]args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number of Rows : ");
		int rows = sc.nextInt();
		int num1 = 2;
		int num2 = 3;
		for(int i=1;i<=rows;i++){
			for(int j=1;j<=rows;j++){

				if(i%2==0){		
					if(j%2==0)
						System.out.print(num1*j + " ");
					else
						System.out.print(num2*j + " ");
				}
				else{
					if(j%2==0)
						System.out.print(num2*j + " ");
					else
						System.out.print(num1*j + " ");
				}

			}
			System.out.println();
		}
	}		
}

import java.util.Scanner;

class SquarePattern{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number of rows:");
		int rows = sc.nextInt();
		
		//Logic 1
		for(int i = 1; i<=rows; i++){
		
			char ch = (char)(64+rows);
			for(int j = 1; j<=rows; j++){

				if(i%2==1){

					if(j%2==1)
						System.out.print("# ");
					else
						System.out.print(ch--+" ");
				}
				else{

					if(j%2==1)
						System.out.print(ch--+" ");
					else
						System.out.print("# ");
				}
			}
			System.out.println();
		}

		//Logic 2
		/*int num = 1;
		for(int i=0;i<rows;i++){
			int ch = rows;
			for(int j=0;j<rows;j++){
				if(num%2==0)
					System.out.print((char)(64+ch--) + " ");
				else 
					System.out.print("# ");
				num++;
			}
			if(rows%2==0)
			num--;
			System.out.println();
		}*/
	}
}

import java.util.Scanner;

class SquarePattern{

	public static void main(String[]args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number of Rows : ");
		int rows = sc.nextInt();
		for(int i = 1; i<=rows; i++){

			int num = rows;
			for(int j = 1; j<=rows; j++){

				if(i%2==0)
					System.out.print((char)(64+num--) + " ");
				else{
					if(j%2==0)	
						System.out.print((char)(64+num--)+" ");
					else
						System.out.print(num--+" ");
				}
			}
			System.out.println();
		}
	}		
}

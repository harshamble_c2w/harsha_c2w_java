import java.util.Scanner;

class Sum2DArray{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number of rows:");
		int rows = sc.nextInt();
		System.out.println("Enter number of columns:");
		int cols = sc.nextInt();

		int arr[][] = new int[rows][cols];

		System.out.println("Start entering the elements:");
		for(int i = 0; i<arr.length; i++){

			for(int j = 0; j<arr[i].length; j++){

				arr[i][j] = sc.nextInt();
			}
		}
	
		int sum = 0;	
		for(int i = 0; i<arr.length; i++){

			for(int j = 0; j<arr[i].length; j++){

				sum+=arr[i][j];
			}
		}

		System.out.println("\nSum of array = "+sum);
	}
}

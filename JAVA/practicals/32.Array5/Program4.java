import java.util.Scanner;

class SumOddRow2DArray{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number of rows:");
		int rows = sc.nextInt();
		System.out.println("Enter number of columns:");
		int cols = sc.nextInt();

		int arr[][] = new int[rows][cols];

		System.out.println("Start entering elements:");
		for(int i = 0; i<arr.length; i++){

			for(int j = 0; j<arr[i].length; j++){

				arr[i][j] = sc.nextInt();
			}
		}
		
		for(int i = 0; i<arr.length; i++){

			if(i%2==1)
				continue;
			int sum = 0;
			for(int j = 0; j<arr[i].length; j++){

				sum+=arr[i][j];
			}
			System.out.println("\nSum of row "+(i+1)+" = "+sum);
		}
	}
}

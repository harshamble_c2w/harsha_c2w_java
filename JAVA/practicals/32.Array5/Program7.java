import java.util.Scanner;

class SumPrimaryDiagonal2DArray{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number of rows:");
		int rows = sc.nextInt();
		System.out.println("Enter number of columns:");
		int cols = sc.nextInt();

		int arr[][] = new int[rows][cols];

		System.out.println("Start entering elements:");
		for(int i = 0; i<arr.length; i++){

			for(int j = 0; j<arr[i].length; j++){

				arr[i][j] = sc.nextInt();
			}
		}

		int product = 1;
		for(int i = 0; i<arr.length; i++){

			for(int j = 0; j<arr[i].length; j++){

				if(i==j)
					product*=arr[i][j];
			}
		}
		System.out.println("\nProduct of primary diagonal: "+product);
	}
}

import java.util.Scanner;

class SumEachColumn2DArray{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number of rows:");
		int rows = sc.nextInt();
		System.out.println("Enter number of columns:");
		int cols = sc.nextInt();

		int arr[][] = new int[rows][cols];

		System.out.println("Start entering elements:");
		for(int i = 0; i<arr.length; i++){

			for(int j = 0; j<arr[i].length; j++){

				arr[i][j] = sc.nextInt();
			}
		}
		
		for(int i = 0; i<arr.length; i++){

			int sum = 0;
			for(int j = 0; j<arr[i].length; j++){

				sum+=arr[j][i];
			}
			System.out.println("\nSum of column "+(i+1)+" = "+sum);
		}
	}
}

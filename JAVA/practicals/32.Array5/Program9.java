import java.util.Scanner;

class ProductOfSumOfPrimarySecondary2DArray{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number of rows:");
		int rows = sc.nextInt();
		System.out.println("Enter number of columns:");
		int cols = sc.nextInt();

		int arr[][] = new int[rows][cols];

		System.out.println("Start entering elements:");
		for(int i = 0; i<arr.length; i++){

			for(int j = 0; j<arr[i].length; j++){

				arr[i][j] = sc.nextInt();
			}
		}
		
		int sumP = 0;
		int sumS = 0;
		for(int i = 0; i<arr.length; i++){

			for(int j = 0; j<arr[i].length; j++){

				if(i==j)
					sumP+=arr[i][j];
				if((arr.length-1-i)==j)
					sumS+=arr[i][j];
			}
		}

		System.out.println("\nProduct of Sum of Primary and Secondary Diagonal is: "+(sumP*sumS));
	}
}

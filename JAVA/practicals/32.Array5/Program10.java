import java.util.Scanner;

class CornerElements2DArray{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number of rows:");
		int rows = sc.nextInt();
		System.out.println("Enter number of columns:");
		int cols = sc.nextInt();

		int arr[][] = new int[rows][cols];

		System.out.println("Start entering elements:");
		for(int i = 0; i<arr.length; i++){

			for(int j = 0; j<arr[i].length; j++){

				arr[i][j] = sc.nextInt();
			}
		}
		
		System.out.println("\nCorner Elements:");
		for(int i = 0; i<arr.length; i++){

			if(i!=0 || i!=arr.length-1)
				continue;
			for(int j = 0; j<arr[i].length; j++){

				if(j==0 || j==arr[i].length-1)
					System.out.print(arr[i][j]+" ");
			}
		}
	}
}

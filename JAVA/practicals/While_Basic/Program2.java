//O.P.
//numbers divisible by 5 in range 50 - 10

class Div5{
	public static void main(String[] args){

		int num = 50;
		while(num >= 10){
			if(num%5 == 0)
				System.out.println(num);
			num--;
		}
	}
}

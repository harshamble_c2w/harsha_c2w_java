//O.P
//print numbers in range 100 to 24 which are divisible by 4 & 5

class Div_4_5{
	public static void main(String[] args){

		int num = 100;
		while(num>=24){

			if(num%4 == 0 && num%5 == 0)
				System.out.println(num);
			num--;
		}
	}
}

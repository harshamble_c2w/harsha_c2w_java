//O.P
//print countdown of days to submit the assignment

class Countdown{
	public static void main(String[] args){

		int days = 7;
		while(days>=0){
			System.out.print(days+" days remaining");
			if(days == 0)
				System.out.print(" Assignment is overdue");
			System.out.println();
			days--;
		}
	}
}

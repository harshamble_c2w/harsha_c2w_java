//O.P
//sum of odd numbers from 150 to 101

class Sum_odd{
	public static void main(String[] args){

		int sum = 0;
		int cntr = 150;
		while(cntr>=101){
			if(cntr%2 != 0)
				sum = sum + cntr;
			cntr--;
		}
		System.out.println(sum);
	}
}

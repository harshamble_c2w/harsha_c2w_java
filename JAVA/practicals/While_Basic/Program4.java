// O.P
//square of first 10 natural numbers

class Square_10{
	public static void main(String[] args){

		int num = 10;
		while(num>=1){
			System.out.println(num*num);
			num--;
		}
	}
}

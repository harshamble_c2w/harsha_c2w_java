//O.P
//print cube of first 10 natural numbers

class Cube_10{
	public static void main(String[] args){

		int num = 1;
		while(num<=10){
			System.out.println(num*num*num);
			num++;
		}
	}
}

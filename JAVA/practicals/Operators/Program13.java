class ShiftOperatorDemo{
	public static void main(String[] args){
		int x = 15;	//0000 1111
		int y = x>>4;	//0000 0000
		int z = x<<4;	//1111 0000
		System.out.println(x);	//15
		System.out.println(y);	//0
		System.out.println(z);	//240
	}
}

class AssignmentDemo{
	public static void main(String[] args){
		int x = 10;
		int y = 5;
		System.out.println("Initial value of x: "+x);
		System.out.println("Initial value of y "+y);
		System.out.println("After x+=3: "+(x+=3));	//13
		System.out.println("After y-=2: "+(y-=2));	//3
		System.out.println("After x*=2: "+(x*=2));	//26
		System.out.println("After y/=3: "+(y/=3));	//1
		System.out.println("After x%=5: "+(x%=5));	//1
	}
}

class C2W{
	public static void main(String[] args){
		int num = 100;
		num--;				//99 0110 0011
		System.out.println(num<<1);	//1100 0110 => 198
		num=num>>>1;			//0011 0001 => 49
		num+=3;				//52
		System.out.println(num);
	}
}

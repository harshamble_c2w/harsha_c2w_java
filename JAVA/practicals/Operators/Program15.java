class UnaryTrial{
	public static void main(String[] args){
		int x = 19;
		System.out.println(x++ + x++);		//19+20 = 39
		System.out.println(++x + x++ + ++x);	//21+23+24 = 68
	}
}

class BitwiseDemo{
	public static void main(String[] args){
		int x = 5;	//0000 0101
		int y = 3;	//0000 0011
		System.out.println("x: "+x);
		System.out.println("y: "+y);
		System.out.println("Bitwise AND: "+(x&y));	//0000 0001
		System.out.println("Bitwise OR: "+(x|y));	//0000 0111
		System.out.println("Bitwise XOR: "+(x^y));	//0000 0110
		System.out.println("Left shift of x: "+(x<<2));	//0001 0100
		System.out.println("Right shift of x: "+(x>>2));//0000 0001
	}
}

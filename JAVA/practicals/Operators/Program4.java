class LogicalDemo{
	public static void main(String[] args){
		boolean x = true;
		boolean y = false;
		System.out.println("x: "+x);
		System.out.println("y: "+y);
		System.out.println("Logical AND: "+(x&&y));
		System.out.println("Logical OR: "+(x||y));
		System.out.println("Logical NOT of x: "+(!x));
		System.out.println("Logical NOT of y: "+(!y));
	}
}

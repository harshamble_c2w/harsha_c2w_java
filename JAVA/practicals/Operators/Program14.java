//Arithmetic: + - * / %
//Unary: + - ++ --
//Assignment: = += -= *= /= %=
//Relational: == != > >= < <=
//Logical: && || !
//Bitwise: & | ^ << >> >>>

class OperatorsDemo{
	public static void main(String[] args){
		int marks_Sub1 = 70;
		int marks_Sub2 = 80;
		int marks_Sub3 = 32;
		int total_marks = marks_Sub1 + marks_Sub2;
		total_marks+=marks_Sub3;
		int average_marks = total_marks/3;
		System.out.println("Marks of subject1: "+marks_Sub1);
		System.out.println("Marks of subject2: "+marks_Sub2);
		System.out.println("Marks of subject3: "+marks_Sub3);
		System.out.println("Total marks: "+total_marks);		//182
		System.out.println("Average marks: "+(average_marks));		//60
		if((marks_Sub1>40)&&(marks_Sub2>40)&&(marks_Sub3>40)){
			System.out.println("Congratulations! You Passed!");
		}
		else{
			System.out.println("You Failed!");
		}
	}
}

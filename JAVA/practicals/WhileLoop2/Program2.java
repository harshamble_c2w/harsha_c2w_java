//print digits not divisible by 3

class NotDiv_3{

	public static void main(String[] args){

		int num = 2569185;
		System.out.print("Digits not divisible by 3 are: ");
		while(num>0){

			int temp = num%10;
			if(temp%3 != 0)
				System.out.print(temp+" ");
			num/=10;
		}
	}
}

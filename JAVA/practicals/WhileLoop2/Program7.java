//print sum of even digits

class Sum_Even{

	public static void main(String[] args){

		int num = 256985;
		int sum = 0;
		while(num>0){
			int temp = num%10;
			if(temp%2 == 0)
				sum+=temp;
			num/=10;
		}
		System.out.println("Sum of even digits is: "+sum);
	}
}

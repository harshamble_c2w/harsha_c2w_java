//print sum of square of odd digits

class Sum_Square_Odd{

	public static void main(String[] args){

		int num = 2469185;
		int sum = 0;
		while(num>0){
			int temp = num%10;
			if(temp%2 != 0)
				sum+=temp*temp;
			num/=10;
		}
		System.out.println(sum);
	}
}

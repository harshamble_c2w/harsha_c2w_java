//print digits in number divisible by 2

class Div_2{

	public static void main(String[] args){

		int num = 2569185;
		System.out.print("Digits divisible by 2 are: ");
		while(num>0){

			int temp = num%10;
			if(temp%2 == 0 && temp!=0)
				System.out.print(temp+" ");
			num/=10;
		}
	}
}

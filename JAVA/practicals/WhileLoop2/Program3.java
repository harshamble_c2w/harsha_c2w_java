//digits divisible by 2 or 3 

class Div_2_3{

	public static void main(String[] args){

		int num = 436780521;
		System.out.print("Digits divisible by 2 or 3 are: ");
		while(num>0){

			int temp = num%10;
			if((temp%2 == 0 || temp%3 == 0) && (temp!=0))
				System.out.print(temp+" ");
			num/=10;
		}
	}
}


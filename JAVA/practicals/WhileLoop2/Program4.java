//print square of odd digits

class Square_Odd{

	public static void main(String[] args){

		int num = 256985;
		while(num>0){

			int temp = num%10;
			if(temp%2 != 0)
				System.out.print(temp*temp+" ");
			num/=10;
		}
	}
}

//print product of odd digits

class Product_Odd{

	public static void main(String[] args){

		int num = 256985;
		int prod = 1;
		while(num>0){
			int temp = num%10;
			if(temp%2 != 0)
				prod*=temp;
			num/=10;
		}
		System.out.println("Product of odd digits is: "+prod);
	}
}

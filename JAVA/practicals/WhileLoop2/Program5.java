//print cubes of even digits

class Cube_Even{

	public static void main(String[] args){

		int num = 216985;
		while(num>0){

			int temp = num%10;
			if(temp%2 == 0 && temp!=0)
				System.out.print(temp*temp*temp+" ");
			num/=10;
		}
	}
}

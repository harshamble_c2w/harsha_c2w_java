//product of digits

class Product_Digits{

	public static void main(String[] args){

		int num = 234;
		int prod = 1;
		while(num>0){
			int temp = num%10;
			prod*=temp;
			num/=10;
		}
		System.out.println(prod);
	}
}


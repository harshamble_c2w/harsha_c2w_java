class IfElseDemo{
	public static void main(String[] args){

		char ch = 97;
		System.out.println(ch);	//a
		if(ch == 'a'){
			ch+=3;		//d
			System.out.println(ch--);	//d
		}
		else{
			System.out.println(ch++);
		}
		System.out.println(ch+=5);		//h
	}
}

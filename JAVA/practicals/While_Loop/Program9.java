class WhileDemo{

	public static void main(String[] args){

		int num = 214367689;
		int oddCnt = 0;
		int evenCnt = 0;
		while(num>0){

			int rem = num% 10;
			if(rem%2==0)
				evenCnt++;
			else
				oddCnt++;
			num/=10;
		}
		System.out.println("Odd count: "+oddCnt);
		System.out.println("Even count: "+evenCnt);
	}
}

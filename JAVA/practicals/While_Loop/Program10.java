class WhileDemo{

	public static void main(String[] args){

		long num = 9307922405l;
		int sum = 0;
		long og = num;
		while(num>0){

			long rem = num % 10;
			sum+=rem;
			num/=10;
		}
		System.out.println("Sum of digits in "+og+" is "+sum);
	}
}

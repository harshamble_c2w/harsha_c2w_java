//even odd

class EvenOdd{

	public static void main(String[] args){

		int num = 15;
		if(num%2 == 0){
			System.out.println(num+" is even number");
		}
		else if(num%2 == 1){
			System.out.println(num+" is odd number");
		}
		else{
			System.out.println("Invalid input");
		}
	}
}

//divisible by 3 or 7

class Div3or7{

	public static void main(String[] args){

		int num = 17;

		if(num % 3 == 0 && num % 7 == 0){
			System.out.println(num+" is divisible by 3 and 7");
		}
		else if(num % 3 == 0){
			System.out.println(num+" is divisible by 3 only");
		}
		else if(num % 7 == 0){
			System.out.println(num+" is divisible by 7 only");
		}
		else{
			System.out.println(num+" is neither divisible by 3 nor by 7");
		}
	}
}

//char is uppercase or lowercase


class UpperLowerCase{

	public static void main(String[] args){

		char ch = '$';

		if(ch >= 'a' && ch <= 'z'){
			System.out.println(ch+" is lowercase");
		}
		else if(ch >= 'A' && ch <= 'Z'){
			System.out.println(ch+" is uppercase");
		}
		else{
			System.out.println(ch+" is neither uppercase nor lowercase");
		}
	}
}

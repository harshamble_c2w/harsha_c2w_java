//max of 2 distinct hardcoded numbers

class MaxOf2{

	public static void main(String[] args){

		int num1 = 23;
		int num2 = 30;

		System.out.println("num1 = "+num1);
		System.out.println("num2 = "+num2);

		if(num1 > num2){
			System.out.println(num1+" is maximum between "+num1+","+num2);
		}
		else if(num1 < num2){
			System.out.println(num2+" is maximum between "+num1+","+num2);
		}
		else{
			System.out.println("Invalid input");
		}
	}
}

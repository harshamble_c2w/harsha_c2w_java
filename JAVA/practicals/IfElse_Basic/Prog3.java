//num greater than 10 or not


class EvenCompare10{
	
	public static void main(String[] args){

		int num = 9;
		
		if(num % 2 == 0 && num > 10){
			System.out.println(num+" is even number and greater than 10");
		}
		else if(num % 2 == 1 && num > 10){
			System.out.println(num+" is odd number and greater thean 10");
		}
		else if(num % 2 == 0 && num < 10){
			System.out.println(num+" is even number and less than 10");
		}
		else if(num % 2 == 1 && num < 10){
			System.out.println(num+" is odd number and less than 10");
		}
		else if(num == 10){
			System.out.println(num+" is equal to 10");
		}
		else{
			System.out.println("Invalid input");
		}
	}
}

import java.util.Scanner;

class ArrayDemo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number of employees:");
		int size = sc.nextInt();
		int empAge[] = new int[size];
		System.out.println("Enter the age of employees:");
		for(int i  = 0; i<empAge.length; i++){
			int temp = sc.nextInt();
			while(temp<18 || temp>60){
				System.out.println("Invalid age, please enter age between 18 to 60");
				temp = sc.nextInt();
			}
			empAge[i] = temp;
		}
		System.out.println("The ages of employees are:");
		for(int i = 0; i<empAge.length; i++){
			System.out.print(empAge[i]+" ");
		}
		System.out.println();
	}
}

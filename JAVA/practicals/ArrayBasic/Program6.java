import java.util.Scanner;

class ArrayDemo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array:");
		int size = sc.nextInt();
		char arr[] = new char[size];
		System.out.println("Enter the elements:");
		for(int i  = 0; i<arr.length; i++){

			arr[i] = sc.next().charAt(0);
		}
		for(int i = 0; i<arr.length; i++){
			System.out.print(arr[i]+" ");
		}
		System.out.println();
	}
}

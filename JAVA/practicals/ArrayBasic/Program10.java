import java.util.Scanner;

class ArrayDemo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number of subjects:");
		int size = sc.nextInt();
		int studMarks[] = new int[size];
		System.out.println("Enter the marks:");
		for(int i  = 0; i<studMarks.length; i++){

			studMarks[i] = sc.nextInt();
		}
		System.out.println();
		System.out.println("The marks of subjects are as follows");
		for(int i = 0; i<studMarks.length; i++){
			System.out.println("Subject"+(i+1)+": "+studMarks[i]+" ");
		}
	}
}

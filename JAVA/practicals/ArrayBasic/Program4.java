import java.util.Scanner;

class ArrayDemo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array:");
		int size = sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Enter the elements:");
		for(int i  = 0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}
		int oddSum = 0;
		for(int i = 0; i<arr.length; i++){
			if(arr[i]%2==1)
				oddSum+=arr[i];
		}
		System.out.println("Sum of odd elements: "+oddSum);
	}
}

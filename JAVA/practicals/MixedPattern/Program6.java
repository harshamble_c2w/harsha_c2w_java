import java.io.*;

class MixedPattern{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the number of rows: ");
		int rows = Integer.parseInt(br.readLine());
		for(int i = 1; i<=rows; i++){

			for(int j = 1; j<=i; j++){

				if(i%2==0)
					System.out.print(rows+1-j+" ");
				else
					System.out.print((char)(97+rows-j)+" ");
			}
			System.out.println();
		}
	}
}

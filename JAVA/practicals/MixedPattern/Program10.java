import java.io.*;

class MixedPattern{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter a number: ");
		long num = Long.parseLong(br.readLine());
		long temp = num;
		long rev = 0;
		while(temp>0){

			long rem = temp%10;
			rev = rev*10+rem;
			temp/=10;
		}
		while(rev>0){
			long rem = rev%10;
			if(rem%2==1)
				System.out.print(rem*rem+", ");
			rev/=10;
		}
		System.out.println();
	}
}

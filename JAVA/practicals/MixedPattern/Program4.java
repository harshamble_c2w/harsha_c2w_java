import java.io.*;

class MixedPattern{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the number of rows: ");
		int rows = Integer.parseInt(br.readLie());
		for(int i = 1; i<=rows; i++){

			int temp = rows+1-i;
			for(int j = 1; j<=i; j++){

				System.out.print(temp*j+" ");
			}
			System.out.println();
		}
	}
}

import java.util.*;

class MixedPattern{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number of rows: ");
		int rows = sc.nextInt();
		int num = 1;
		for(int i = 1; i<=rows; i++){

			for(int j = 1; j<=rows+1-i; j++){

				System.out.print(num*2+" ");
				num++;
			}
			System.out.println();
		}
	}
}

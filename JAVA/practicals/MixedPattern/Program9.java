import java.util.*;

class MixedPattern{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number of rows: ");
		int rows = sc.nextInt();
		
		for(int i = 1; i<=rows; i++){

			for(int j = 1; j<=rows+1-i; j++){

				if(i%2==0)
					System.out.print((char)(66+rows-i-j)+" ");
				else
					System.out.print(j+" ");
			}
			System.out.println();
		}
	}
}

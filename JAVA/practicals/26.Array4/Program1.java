//print average of elements in array

import java.util.Scanner;

class Average{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size of array:");
		int size = sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Enter array elemnts:");
		for(int i = 0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}
		
		int sum = 0;
		for(int i = 0; i<arr.length; i++){

			sum+=arr[i];
		}
		System.out.println("Array elements' average is: "+sum/arr.length);
	}
}

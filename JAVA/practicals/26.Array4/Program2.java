//difference between minimum element and maximum element in array

import java.util.Scanner;

class DiffMinMax{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size of array:");
		int size;
		while(true){
			size = sc.nextInt();
			if(size>0)
				break;
			System.out.println("Enter size greater than 0!");
		}
		int arr[] = new int[size];
		System.out.println("Enter array elemnts:");
		for(int i = 0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}

		int minElement = arr[0];
		int maxElement = arr[0];
		for(int i = 0; i<arr.length; i++){

			if(arr[i]>maxElement)
				maxElement = arr[i];
			else if(arr[i]<minElement)
				minElement = arr[i];
		}
		System.out.println("The difference between the minimum and maximum element is: "+(maxElement-minElement));
	}
}

//replace elements in range of 'a to z' with #

import java.util.Scanner;

class Replace{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size of array:");
		int size = sc.nextInt();
		char arr[] = new char[size];
		System.out.println("Enter array elemnts:");
		for(int i = 0; i<arr.length; i++){

			arr[i] = sc.next().charAt(0);
		}

		for(int i = 0; i<arr.length; i++){

			if(arr[i]<=97 || arr[i]>=122)
				arr[i] = '#';
		}
		
		System.out.println("Array:");
		for(int i = 0; i<arr.length; i++){

			System.out.println(arr[i]);
		}
	}
}

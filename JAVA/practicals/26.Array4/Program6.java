//count the vowels and consonants

import java.util.Scanner;

class VowelsConsonants{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size of array:");
		int size = sc.nextInt();
		char arr[] = new char[size];
		System.out.println("Enter array elemnts:");
		for(int i = 0; i<arr.length; i++){

			arr[i] = sc.next().charAt(0);
		}

		int vowelCount = 0;
		int consonantCount = 0;
		for(int i = 0; i<arr.length; i++){

			if((arr[i]>=65 && arr[i]<=90) || (arr[i]>=97 && arr[i]<=122))
				if(arr[i]=='a' || arr[i]=='e' || arr[i]=='i' || arr[i]=='o' || arr[i]=='u' || arr[i]=='A' || arr[i]=='E' || arr[i]=='I' || arr[i]=='O' || arr[i]=='U')
					vowelCount++;
				else
					consonantCount++;
		}
		System.out.println("Count of vowels: "+vowelCount+"\nCount of consonants: "+consonantCount);
	}
}

//print characters in array which come before user given character

import java.util.Scanner;

class ComeBefore{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size of array:");
		int size = sc.nextInt();
		char arr[] = new char[size];
		System.out.println("Enter array elemnts:");
		for(int i = 0; i<arr.length; i++){

			arr[i] = sc.next().charAt(0);
		}

		System.out.println("Enter the character key:");
		char key = sc.next().charAt(0);
		System.out.println("Array:");
		for(int i = 0; i<arr.length; i++){

			if(arr[i]==key)
				break;
			System.out.println(arr[i]);
		}
	}
}

//to check whether user given number occurs more than 2 times or equals to 2 times

import java.util.Scanner;

class OccurTwoTimes{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size of array:");
		int size = sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Enter array elemnts:");
		for(int i = 0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}

		System.out.println("Enter a number to check:");
		int key = sc.nextInt();
		int count = 0;
		for(int i = 0; i<arr.length; i++){

			if(arr[i] == key)
				count++;
		}

		if(count == 2)
			System.out.println(key+" occurs exactly 2 times in the array");
		else if(count>2)
			System.out.println(key+" occurs more than 2 times in the array");
		else
			System.out.println(key+" occurs less than 2 times in the array");
	}
}

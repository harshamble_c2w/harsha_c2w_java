//occurence of user given character

import java.util.Scanner;

class Occurence{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size of array:");
		int size = sc.nextInt();
		char arr[] = new char[size];
		System.out.println("Enter array elemnts:");
		for(int i = 0; i<arr.length; i++){

			arr[i] = sc.next().charAt(0);
		}

		System.out.println("Enter the character to check:");
		char key = sc.next().charAt(0);
		int count = 0;
		for(int i = 0; i<arr.length; i++){

			if(arr[i]==key)
				count++;
		}
		if(count==0)
			System.out.println(key+" is not present in the array");
		else
			System.out.println(key+" occurs "+count+" times in the given array");
	}
}

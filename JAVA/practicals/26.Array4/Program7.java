//convert lowercase characters to uppercase characters

import java.util.Scanner;

class LowerToUpperCase{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size of array:");
		int size = sc.nextInt();
		char arr[] = new char[size];
		System.out.println("Enter array elemnts:");
		for(int i = 0; i<arr.length; i++){

			arr[i] = sc.next().charAt(0);
		}

		for(int i = 0; i<arr.length; i++){

			if(arr[i]>=97 && arr[i]<=122){

				arr[i]-=32;
			}
		}

		for(int i = 0; i<arr.length; i++){

			System.out.print(arr[i]+" ");
		}
		System.out.println();
	}
}

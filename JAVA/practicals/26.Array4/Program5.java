//reverse array

import java.util.Scanner;

class ReverseArray{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size of array:");
		int size = sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Enter array elemnts:");
		for(int i = 0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}

		int arrRev[] = new int[size];
		for(int i = 0; i<arr.length; i++){

			arrRev[i] = arr[arr.length-i-1];
		}

		System.out.println("Reverse array:");
		for(int i = 0; i<arrRev.length; i++){

			System.out.println(arrRev[i]);
		}
	}
}

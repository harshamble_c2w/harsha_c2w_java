//find 2nd largest element in array

import java.util.Scanner;

class SecondMax{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size of array:");
		int size;
		while(true){
			size = sc.nextInt();
			if(size>0)
				break;
			System.out.println("Enter array size greater than 0!");
		}
		int arr[] = new int[size];
		System.out.println("Enter array elemnts:");
		for(int i = 0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}
		
		int maxElement = arr[0];
		for(int i = 0; i<arr.length; i++){

			if(arr[i]>maxElement)
				maxElement = arr[i];
		}
		
		int maxElement2 = 0;
		for(int i = 0; i<arr.length; i++){

			if(arr[i]>maxElement2 && arr[i]!=maxElement)
				maxElement2 = arr[i];
		}

		System.out.println("The second largest element in the array is: "+maxElement2);
	}
}

//if array size is odd & >= 5 print odd elements
//else print even elements

import java.util.Scanner;

class ConditionedPrint{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array: ");
		int size = sc.nextInt();
		while(size<=0){
			System.out.println("Please enter the array size greater than 0: ");
			size = sc.nextInt();
		}
		int arr[] = new int[size];
		
		System.out.println("Start entering the elements: ");
		for(int i = 0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}

		if(arr.length%2==1 && arr.length>=5){
			for(int i = 0; i<arr.length; i++){
	
				if(arr[i]%2==1)		
					System.out.print(arr[i]+" ");
			}
		}
		else{
			for(int i = 0; i<arr.length; i++){

				if(arr[i]%2==0)
					System.out.print(arr[i]+" ");
			}
		}
		System.out.println();
	}
}

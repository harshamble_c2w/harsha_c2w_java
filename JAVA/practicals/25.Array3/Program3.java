//find number of occurences of a specific number in array
//print number of occurences

import java.util.Scanner;

class NoOfOccurences{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array: ");
		int size = sc.nextInt();
		while(size<=0){
			System.out.println("Please enter the array size greater than 0: ");
			size = sc.nextInt();
		}
		int arr[] = new int[size];
		
		System.out.println("Start entering the elements: ");
		for(int i = 0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}
		
		System.out.println("Specific Number: ");
		int key = sc.nextInt();
		int count = 0;
		
		for(int i = 0; i<arr.length; i++){
			
			if(arr[i]==key)
				count++;
		}

		if(count>0)
			System.out.println("Number "+key+" occured "+count+" times in the array");
		else
			System.out.println("Number "+key+" not found in the array");
	}
}

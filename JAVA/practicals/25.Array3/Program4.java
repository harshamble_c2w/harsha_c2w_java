//convert all even numbers into 0 and all odd numbers into 1

import java.util.Scanner;

class ConvertElements{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array: ");
		int size = sc.nextInt();
		while(size<=0){
			System.out.println("Please enter the array size greater than 0: ");
			size = sc.nextInt();
		}
		int arr[] = new int[size];
	
		System.out.println("Start entering the elements:");	
		for(int i = 0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}

		for(int i = 0; i<arr.length; i++){
			
			if(arr[i]%2==0)
				arr[i]=0;
			else
				arr[i]=1;
		}

		for(int i = 0; i<arr.length; i++){
		
			System.out.print(arr[i]+" ");
		}
		System.out.println();
	}
}

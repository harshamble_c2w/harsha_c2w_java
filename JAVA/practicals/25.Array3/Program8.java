//composite numbers in array

import java.util.Scanner;

class Composite{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array: ");
		int size = sc.nextInt();
		while(size<=0){
			System.out.println("Please enter the array size greater than 0: ");
			size = sc.nextInt();
		}
		int arr[] = new int[size];
		
		System.out.println("Start entering the elements: ");
		for(int i = 0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}
		
		for(int i = 0; i<arr.length; i++){
			
			int temp = arr[i];
			int factorCount = 0;
			while(temp>0){

				if(arr[i]%temp==0)
					factorCount++;
				temp--;
			}
			if(factorCount>2)
				System.out.print(arr[i]+" ");
		}
		System.out.println();
	}
}

//Add 15 in all elements of array & print it

import java.util.Scanner;

class Add15{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array: ");
		int size = sc.nextInt();
		while(size<=0){
			System.out.println("Please enter the array size greater than 0: ");
			size = sc.nextInt();
		}
		int arr[] = new int[size];
		System.out.println("Start entering the elements: ");
		for(int i = 0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}
		for(int i = 0; i<arr.length; i++){
			
			System.out.print(arr[i]+15+" ");
		}
		System.out.println();
	}
}

//prime numbers in array

import java.util.Scanner;

class Prime{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array: ");
		int size = sc.nextInt();
		while(size<=0){
			System.out.println("Please enter the array size greater than 0: ");
			size = sc.nextInt();
		}
		int arr[] = new int[size];
		
		System.out.println("Start entering the elements: ");
		for(int i = 0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}
		
		for(int i = 0; i<arr.length; i++){
			
			int temp = arr[i]-1;
			int factorCount = 2;

			if(temp<1)
				continue;
			while(temp>1){

				if(arr[i]%temp==0)
					factorCount++;
				temp--;
			}
			if(factorCount==2)
				System.out.print(arr[i]+" ");
		}
		System.out.println();
	}
}

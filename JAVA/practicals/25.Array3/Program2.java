//find first occurences of a number
//print index of first occurence

import java.util.Scanner;

class FirstOccurence{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array: ");
		int size = sc.nextInt();
		while(size<=0){
			System.out.println("Please enter the array size greater than 0: ");
			size = sc.nextInt();
		}
		int arr[] = new int[size];
		System.out.println("Start entering the elements: ");
		for(int i = 0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}
		System.out.println("Enter the number you want to search:");
		int key = sc.nextInt();
		int found = 0;
		for(int i = 0; i<arr.length; i++){
			
			if(arr[i]==key){
				System.out.println("Number "+key+" first occured at index: "+i);
				found = 1;
				break;
			}
		}
		if(found==0)
			System.out.println("Number "+key+" not found in the array");
	}
}
